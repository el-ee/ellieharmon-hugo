---
title: "Home"
date: 2019-02-09T15:49:00-08:00
menu:
    main:
        title: "Home"
        weight: 1000
---

![Me and Luke smiling in the sun at Sunriver, December 2018](/images/sunriver2018.jpg)

I research, teach, and write about people’s encounters with technology
 [<i class="fal fa-external-link"></i> work website](https://web.cecs.pdx.edu/~harmon8/)

I live with my partner, [Luke Olbrish][luke], in Portland, Oregon, where
we spend lots of time at Thorns & Timbers soccer games, and occasionally
get out and explore the great PNW outdoors.

I thru-hiked the
[PCT in 2013](http://www.pcta.org/discover-the-trail/long-distance-hiking/2600-miler-list/)
and the [AT in 2008](http://www.appalachiantrail.org/2000-milers).

[Luke]: http://lukeolbrish.com/
