---
author: ellie
comments: true
date: 2013-05-14
layout: post
slug: day-24-little-bear-springs-horsecamp-to-footbridge-deep-creek
title: 'Day 24: Little Bear Springs Horsecamp to Footbridge @ Deep Creek'
wordpress_id: 2246
tags:
- PCT
---

[caption id="attachment_2248" align="aligncenter" width="525"][![Breaking camp in the morning](http://ellieharmon.com/wp-content/uploads/IMG_20130514_0655161-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130514_0655161.jpg) Breaking camp in the morning -- it's starting to get crowded out here![/caption]

Even more people came in last night after we crawled into our tent -- quite the scene this morning at camp! Seems like people that piled up in Idyllwild and Big Bear might all be accumulating on similar schedules now back out on the trail. Towns seem to sync people up.

<!-- more -->

[caption id="attachment_2249" align="aligncenter" width="525"][![Are you highway legal?](http://ellieharmon.com/wp-content/uploads/IMG_20130514_075427-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130514_075427.jpg) Are you highway legal? (Spent a long time today walking near jeep/dirt bike roads.)[/caption]

A rather unintentionally long day today, ended up doing nearly 24 miles. I now have THREE blisters -- the blister between my big toe and first toe on the left foot now has a twin on my right foot, and the fresh skin on the left one is re-blistered, now too... and a blister on my heel! I never get blisters! What is this?! (Heat + shoes that must be too small despite being a men's 10! I didn't think my feet would grow that much after they already expanded on the AT, but apparently I thought WRONG.)

[caption id="attachment_2254" align="aligncenter" width="525"][![Lunch party at Deep Creek](http://ellieharmon.com/wp-content/uploads/IMG_20130514_140438-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130514_140438.jpg) Lunch party at Deep Creek[/caption]

In any case, we had a super awesome morning today; arrived at the first Deep Creek crossing right around lunchtime, and hung out under the bridge for a long time eating, and chatting, and swimming with many of the folks we camped with last night. [Carrot](http://carrotquinn.wordpress.com/), part of the big crew from Portland, shared some fresh celery with us! CRUNCHY! YUM! And showed us a cool video that she (or maybe her friend, Angela?) took on someone's phone of a tarantula hawk -- a wasp that paralyzes tarantulas and then carries them off to lay eggs inside them! I can't find their movie, but here is a similar one from Zion NP:

[embed]http://www.youtube.com/watch?v=3sWiPnl-Zss[/embed]

We also had a little cell service under the bridge, so I got to check in with Mom about her stay at Luke's apartment in SF. It's funny being on the other side of this accessibility thing. For a change, I was the one wondering about how HER travels were going!

[caption id="attachment_2255" align="aligncenter" width="525"][![Signs on the trail directing us across deep creek](http://ellieharmon.com/wp-content/uploads/IMG_20130514_140556-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130514_140556.jpg) Signs on the trail directing us across deep creek[/caption]

[caption id="attachment_2253" align="aligncenter" width="525"][![Road to the left, trail to the right.](http://ellieharmon.com/wp-content/uploads/IMG_20130514_113643-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130514_113643.jpg) Road to the left, trail to the right.[/caption]

[caption id="attachment_2252" align="aligncenter" width="525"][![I thought this sign was cute. There are dead trees everywhere out here!!! But I'm sure the huge swarms of bees in this particular one were happy for its particular existence.](http://ellieharmon.com/wp-content/uploads/IMG_20130514_105154-768x1024.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130514_105154.jpg) I thought this sign was cute. There are dead trees everywhere out here!!! But I'm sure the huge swarms of bees in this particular one were happy for its particular existence.[/caption]

We opted to skip the deep creek hot springs, after reading in all the guidebooks about how gross it is, and because when we walked by there was tons of used toilet paper EVERYWHERE. We stumbled onto a less toilet-paper-infested little creek a couple miles later, and Luke ventured up the hillside to get a refill on our water for the last few miles of hiking.

Ended up camping at a little footbridge across deep creek. Quite the scramble down off the hillside to the flat sandy shores, but totally worth it. Got to do a little wading and clean off my feet! And, it's nice and warm but surprisingly not that buggy down by the fast-moving water. Despite this, there must be bugs somewhere, because there are lots of bats swooping over head. Lots of noises tonight -- water, crickets, frogs, bullfrogs (?) -- all a little strange in 'the desert.'

Water is proving to be quite the challenge out here. Even though we hiked 'along' Deep Creek for almost the whole day, it was only easily accessible a handful of times -- at lunch, at the dirty hot springs, and reachable with some work here under the footbridge -- because the trail is routed up high along the steep hillside. Tomorrow, it looks like more of the same, so we're hoping to do ~23 miles again to a good water source for camping even though my little toes probably won't appreciate it too much. Hoping they dry out a bit overnight.
