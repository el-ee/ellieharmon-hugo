---
author: ellie
comments: true
date: 2013-04-24
layout: post
slug: day-4-cibbets-flats-cg-to-mt-laguna
title: 'Day 4: Cibbets Flats CG to Mt. Laguna'
wordpress_id: 2036
tags:
- PCT
---

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_074716.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_074716.jpg)

The sign and fencing around the campground where we slept last night. (Expect the sign pictures to continue as a theme. There are SO MANY! And yet the ones marking the actual trail are often terrrrrrible with some rationale about how you're supposed to have maps and be responsible for knowledge of the trail's path + too many blazes will mess up the wild & scenic aesthetic...)
<!-- more -->

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_081953.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_081953.jpg)

Enjoying the yucca blooms of various kinds while hiking. Not many cacti yet. Still scrubby bush things most places.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_120320.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_120320.jpg)

PCT markers: each one shows a different set of allowed and disallowed activities, I assume because they have different problems at different locations.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_124249.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_124249.jpg)

Fences like this often line the sides of the trail. There is clearly a lot of land politics in socal. Curious if this continues in other areas as we walk north. And what a map of all the private/public/easements looks like..

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_102647.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_102647.jpg)

A little patch of our first non desert scenery on the trail as we approached mt Laguna!!

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_124239.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_124239.jpg)

Water. A perpetual issue. The [water report](http://pctwater.net) being a literal life saver out here.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_130634.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_130634.jpg)

Put on your seatbelts folks! We are entering our first 'town'-like place!

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_130752.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_130752.jpg)

...and all the (two) restaurants are closed because it is not the weekend. Apparently this is just a day trip place for the big SoCal (sub)urban areas.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_172123.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_172123.jpg)

The outfitter run by a PCT hiker was great and the store and PO at the lodge being the only other stuff around that were open. We hung out on the store porch for a while and even ran into Adam of disco pickle again! Disco Pickle, BTW, is a trio of awesome folks:
[Robin](http://somanymiles.wordpress.com/), [Rachel](http:// irunhikecamp.wordpress.com/), and [Adam](http://mobileoffice78.blogspot.com/)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_183945.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_183945.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_183949.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130424_183949.jpg)

We stayed overnight at the lodge so we can get a hitch back to kickoff tomorrow And while there, we did some laundry for the first time. The desert is SO dusty and dirty. They gave us this handy bucket upon checking, having presumably had one too many dirty hikers clog their pipes/septic system with mud ;)
