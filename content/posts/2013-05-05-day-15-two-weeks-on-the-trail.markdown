---
author: ellie
comments: true
date: 2013-05-05
layout: post
slug: day-15-two-weeks-on-the-trail
title: Day 15 - two weeks on the trail!
wordpress_id: 2127
tags:
- PCT
---

Made it down into idyllwild today and recorded this awesome video for you!

[embed]https://www.youtube.com/watch?v=WAaO0BI2FtY[/embed]
