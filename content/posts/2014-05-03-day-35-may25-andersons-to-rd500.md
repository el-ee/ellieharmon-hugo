---
author: ellie
comments: true
date: 2014-05-03
layout: post
title: '2013-05-25 | Day 35: Andersons to RD 500'
tags:
- PCT
---


Woke up at the Anderson's this morning & had amazing pancakes from Joe. While we ate breakfast, NoDay painted Luke's toes:

[![noday painting luke&#x27;s toes :)](https://farm4.staticflickr.com/3828/11136610363_64e016fe52_c.jpg)](https://flic.kr/p/hY73gv)

*His first pedicure ever, apparently. she said something about it being ok because this isn't the real world. he said he doesn't care too much about norms in the real world either. Note he's got half mile's app open on his phone to start thinking about where we'll hike today.*

When you're leaving Casa de Luna, Terrie Anderson insists on a picture, and then moons you to make sure you smile for the camera!

[![Pleats & co getting mooned!](https://farm6.staticflickr.com/5509/11136462346_2caeeca16d_c.jpg)](https://flic.kr/p/hY6hgu)

Some photos from the hike today:

[![Flowers and trail](https://farm6.staticflickr.com/5545/11136497844_ba167190e0_c.jpg)](https://flic.kr/p/hY6sPw)

*Love this little patch of flowers -- and continue to be amazed at just how fast the trail changes, and how dramatic some of the lines in the landscape can be. Like, this one, field of flowers in the sun, and then a clear line of more shade, no flowers, all grass, different bushes.*

[![second breakfast](https://farm8.staticflickr.com/7295/11136493224_3a919bdbe6_c.jpg)](https://flic.kr/p/hY6rrS)

*Second breakfast with Sagitar, Sagitar's two friends (who just joined him in Agua Dulce), MeHap, and a guy whose name I forget*

[![trail signage](https://farm8.staticflickr.com/7430/11136418825_d35ab664f2_c.jpg)](https://flic.kr/p/hY64k8)

*Signs of past boy scouts. We see lots of things like this dated 70s, 80s -- springs, campsites, etc. Few things newer than this? Boyscouts do less outdoor device projects?*

[![Awesome Sauce! sign](https://farm3.staticflickr.com/2890/11136414525_a18ec6acc8_c.jpg)](https://flic.kr/p/hY633Z)

*A little random pep talk*

[![Metal PCT Marker on a pole](https://farm6.staticflickr.com/5548/11136409705_bb3308348d_c.jpg)](https://flic.kr/p/hY61BT)

*I'm so interested in all the different kinds of trail signage.*

[![PCT through a forest](https://farm8.staticflickr.com/7412/11136431486_b6c1190637_c.jpg)](https://flic.kr/p/hY686q)

*Happy for this shady reprieve in the afternoon. We had a small stretch of really lovely hiking for the "desert"!*

[![PCT on an open field / dirt roads](https://farm4.staticflickr.com/3689/11136426586_20c03217ee_c.jpg)](https://flic.kr/p/hY66CW)

*Walking up to a water source / natural hiker meeting spot at the convergence of these roads*

[![Hikers rest and sort out water plans](https://farm6.staticflickr.com/5512/11136422356_d133cc53ef_c.jpg)](https://flic.kr/p/hY65o1)

*Everyone takes a break, even though there isn't really any shade*

[![Cool views from this ridge line above the big desert](https://farm3.staticflickr.com/2849/11136228705_06f26a569e_c.jpg)](https://flic.kr/p/hY55Pc)

*Cool early evening views from this ridge line above the big desert*

Got to camp early and I wasted all my extra tent time cutting dead skin off my feet. The edges of these blisters  collect dirt like buckets! Camped with Dragonfly and Beaker & Tortuga. They are all asleep. It's not dark yet! Everyone else moved on to get past the 500 mile marker. I guess it makes sense. Today was an easy day, but just as nice to stop early.

[![Getting water out of a cistern](https://farm4.staticflickr.com/3780/11136385075_caba2fe56b_c.jpg)](https://flic.kr/p/hY5Tie)

We got our last water out of a huge concrete cistern with an iron lid -- I wonder if anyone ever closes it? The water tastes AWFUL. I can only imagine its dead animal flavor. We filtered it, of course. But, still. Gross. Definitely full of lots of leaves, etc. Clearly stuff gets in there.

All of the standing water we've passed up before this cistern was probably better! C'est la vie. Nothing else tip a cache. Ten more miles or so. We did 21.25 today between 8:45am - 6:15pm. Great time walking wit Sagitar a little this morning.

I don't think that crowd is going to stop at Hiker Town tomorrow, but maybe Dragonfly and Beaker and Tortuga will. I don't see any point in camping by the aqueduct anyway. Apparently there is some plan to night hike it. I'm pretty down to just knock it out in a day after HT. Twenty-some flat easy miles.

People keep coming by our campsite and are very confused we're not going to keep going to the 500 mile mark. No one else camped here though lots of people stopped and said hi -- beads, Sagitar, two people I don't remember. I have Robin Hood's knife, too. We found it this morning at the Anderson's after he and left. I don't really think anyone will get to Tehachapi much sooner than us -- they'd have to do 3 x 27 mile days, which seems unlikely.
