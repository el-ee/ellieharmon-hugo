---
author: ellie
comments: true
date: 2013-05-17
layout: post
slug: day-27-gobblers-knob-to-wrightwood
title: 'Day 27: Gobbler''s knob to Wrightwood'
wordpress_id: 2296
tags:
- PCT
---

Woke up to fog on both sides of the mountain we're camped on top of. I think it's going to burn away before it gets to us, but still pretty unexpected. I keep being surprised by the weather. I thought this whole section was 'desert.'

Talked a bit with toots and tears while packing up camp and eating breakfast. toots says she broke her no internet on the trail rule last night for the second time. As i can attest to as well, she was talking about how the WordPress app is TERRIBLE for trying to use offline. Often losing posts and messing up pictures. So, she thought since she was up on a ridge she would just try to connect so she could write her blog post without so much frustration. And she did have service, so blog posting was a success. I say that I killed my battery trying to SMS my mom and sending her pictures of us eating dinner. (Pictures today only after getting to charge the phone in town.).

We met milkman at our lunch break, sitting down near us saying that his GPS says he's off the trail. We were both like pretty sure this is it. But I think he wanted to try to get his app to agree before he kept walking. He's meeting his fiancee in town for a long three day weekend -- the only thing he has scheduled for the whole trail is to be to wright wood by today. No end date pressure etc. So, I can understand his desire to double check. Our app told us we were off trail just yesterday and even though we were pretty sure it was wrong, we stopped and pulled out our paper maps on the hillside to try to figure it out. Adds some stress when these things malfunction. Otherwise we wouldn't have questioned our being 'on trail' -- we just wanted to find out how far we had walked since our McDonalds break. But when trying to get an easy answer for that we ended up getting a little derailled. Still not sure if there had been some kind of reroute, or the app data was just wrong, or the GPS was malfunctioning or what.

So, Lots of tech stuff today I guess..

And then, a long hike down through a ski area. Felt like we were walking around some funny boundaries at times. I guess the trail has to still be walkable even when ski season is happening. But it felt silly today with everything shut down.

We got a quick ride into town with someone who was dropping hikers of at the trail head when we arrived. Apparently she had a couple hours free this afternoon and was planning to down most of it shuttling hikers! She even apologized for not inviting us to get house but she had an early bike ride planned at Huntington Beach tomorrow. I'm like no worries! The ride is excellent! I don't really know why people are so helpful, not sure what they're getting out of it but boy do we appreciate it.

She dropped us off at the hardware store where they maintain a hiker register with contact info for all sorts of trail angels in the front. Lots of people around town seen to let hikers stay in their homes and are willing to give rides if needed etc.

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130517_182556.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130517_182556.jpg)





Nonetheless we opted for a hotel. Big plans to get pizza and make some salt baths for our feet. Didn't want to get stuck in a crowd of hikers at someone's house tonight, need a little quiet and solitude for the evening I think. But a very friendly town!!

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130517_194521.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130517_194521.jpg)





We sorted out our resupply plan, even getting to shop for part of it at a nice little local natural foods store -- we really miss non Safeway/Vons/Kroger grocery stores -- and got some laundry done .. though it resulted in a lost sock. Very sad :(

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130517_171859.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130517_171859.jpg)



Ended the evening with a trip to the local farmers market. Picked up some extra nice (if heavy) goodies for the trail this week and generally enjoyed the live acoustic music and chill atmosphere.
