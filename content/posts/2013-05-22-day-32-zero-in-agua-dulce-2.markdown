---
author: ellie
comments: true
date: 2013-05-23
layout: post
slug: day-32-zero-in-agua-dulce-2
title: 'Day 32: Zero in Agua Dulce [Version 2]'
wordpress_id: 2414
tags:
- PCT
---

I just wrote this post out... and upon hitting publish instead of getting uploaded it seems to have vanished from the face of the earth... so, here we go again. The worst part not being the typing, but the dealing with pictures, choosing them, inserting them, uploading them, remembering what they are in the course of the post when all you can see is the stupid file name that wordpress gives them. This whole using a phone as a computer thing isn't really a very reliable way of doing much of anything, even when there's a custom app for it. Shame on you wordpress, eating my blog posts >.< Also, this is why I would never buy a chromebook.

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1342271.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1342271.jpg)



ANYWAY, after our night at the goat farm, we got totally spoiled today back at the Saufley's where we planned to take a zero day off the trail. We arrived a little before 7am to be greeted by a huge hug from Donna, and the folks in charge set us up in the 'honeymoon suite' in one of the trailers on the property.  I realize a room in a trailer might not sound all /that/ fancy to those of you reading at home, but most hikers here stay in cots set up in tents in the yard, so having a real bed, in a private room is kind of a big deal!!

We also managed to arrive just in time for the van ride to town (about a mile away) where the trail actually goes through and where the restaurants are, so after dropping our packs, we hopped in for a quick ride to breakfast!

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1523561.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1523561.jpg)



Getting back to Hiker Heaven a couple hours later, we picked up some loaner clothes and towels from the meticulously organized containers in the driveway (women's shirts, men's shirts, women's dresses, men's shorts, etc.) and got our names on the (short!) list for a shower. Once we got clean (for the second time in as many days!) it was SO very nice to put on some COTTON t-shirts and comfy pants for a change.

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1055111.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1055111.jpg)



We spent much of the day, hanging out in the Saufley's lovely yard, under the amazing shade of a huge tree. Spending several weeks in the desert makes you much more appreciative of such things! We soaked our weary feet in delightful salt baths, and enjoyed Wildfire's guitar playing and Sharkey's banjo picking while we chatted with other hikers, caught up on journaling, blogging, emailing, and calls to our parents.

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_093242.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_093242.jpg)



[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1342301.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1342301.jpg)



The Saufley's place is known as 'Hiker Heaven' at this time of year -- and for good reason! THe whole driveway, garage and yard is set up with thru-hikers in mind. There are port a potties, big tents with cots for sleeping, the RVs (which, in addition to a few limited rooms for sleeping also have a living area with a TV and huge movie selection, a big charging station for phones etc., a kitchen where you can cook real food that requires multiple pots and pans, a fridge, and a real bathroom with a shower and flush toilet!), wifi as well as a tent set up with laptops for sharing, the aforementioned loaner clothes, towels, a makeshift post office (with incoming packages organized better than most actual post offices, all variety of priority mail packaging, free tape (!), and a computer and printer ready to go with usps.com click-n-ship), and a whole set of information boards.

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1421491.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1421491.jpg)



[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1421531.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1421531.jpg)



The information boards have everything from addresses for mail drops up the trail (and the knowledge that you CAN actually send things out from Kennedy Meadows, which we previously thought was impossible), phone numbers of trail angels who host hikers at their houses or offer rides to/from the trail, freshly printed water reports for you to pick up on your way out,  a register to see who has been around ahead of you, and notices about Cuddles' upcoming cello concert! We were stoked to see he's conveniently playing here, tonight.

Donna Saufley also maintains the official list of names and emails for each year's hikers, so that later on someone can contact everyone else for videos and pictures to contribute to the class video, etc.

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1422051.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1422051.jpg)



[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_141927.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_141927.jpg)



Although we didn't have any packages to pick up here (we decided to resupply out of the local grocery store this time), we did a little mailing out -- some dirt to Christina, and some rejected gear to send home to Katie who is doing an amazing job being our resupply master!

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1449231.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1449231.jpg)



We also signed up for one of the daily REI rides, which took up most of the rest of our afternoon. The trip to the store was kind of a culture shock for everyone -- the traffic on the freeways outside of LA, the housing developments out in the suburbs. One set of banners reading 'Live Green. Save Green' by one big gated subdivision let to quite the eruption of shouting in the car. 'What do these people do, commute to LA?" "Use a lot of gas?" "The sign says it so it must be true." "Do they collect rainwater for their landscaping?" Not much love for the suburban socal lifestyle ;)


[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1711101.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1711101.jpg)



But... a little more love for this piece of civilization.

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1850421.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_1850421.jpg)



The out-of-state folks were pretty entertained by the calorie counts that CA requires restaurnts to post on menus. Makes it a bit terrifying to think that 'normal' people eat here.. but for always-hungry hikers makes for a good game to see who can eat the most (and perhaps get the best calorie count/$). Can't say I'm the biggest fan of the in n out burger but I devoured some animal fries and a shake :)

Quite a bit of talk about ice axes as we have now passed the halfway point of the desert section, and are quickly approaching our last opportunities to send things ahead to ourselves at Kennedy Meadows -- the mentally big (but geographically tiny) outpost that marks the traditional 'start' of 'The Sierra.'  Given the crazy-low precip that California received this winter, Luke and I have decided not to bring either ice axes or microspikes/crampons, and I would say aboutt 90% of hikers are doing the same. But there are a few who seem to be planning to take them along (seemingly inspired by hyper-cautious listserv posts by a certain individual who runs a snow camping/expedition training school).

Back at the Saufley's, we enjoyed an amazing dinner that Karma made to share in the RV kitchen while listening to Cuddle's play the cello. After dinner, we hung out til WAY past hiker midnight (like actual real midnight!) talking with our friends MeHap, Sagittar, Egg, Ole, Veggie.
