---
author: ellie
comments: true
date: 2013-05-02
layout: post
slug: day-12-agua-caliente-to-tule-spring
title: Day 12 - agua caliente to tule spring
wordpress_id: 2118
tags:
- PCT
---

Hot hike this morning. We were thankful to reach mikes place in the afternoon and were surprised to find more than just the water that we'd read about in the water report! Apparently we had just missed lunch but we most enjoyed some leftover potato salad and sodas. Lots of hikers hanging out in the shade and chairs set up around the porch and lawn. People talking and journaling and reading. A very relaxing afternoon break!

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_144919.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_144919.jpg)

<!-- more -->

Lots of talk about blisters. Kushy, who seemed to be running the place, was advocating the thread method to gummie bear who has some scary painful looking feet! The idea is that you pop the blister with a needle and then leave a piece of thread in there to act like a wick and also prevent the blister from healing back up. I have never tried it but it sounds reasonable? Gummie bear wasn't convinced I don't think.

A rather discouraged lady whose name I didn't catch came in about the same time we did and gets in on the blister conversation a little bit. And the conversation turns to one about hiking as a mental not physical challenge - something I've heard a lot and sort of agree with.. But these things are not so easily separable either. K also sharing wisdom on this point: "Will the pain ever stop? No." And then, as someone heads back out towards the trail, "first one to Canada is the loser you know"

These seem to be common beliefs that I've heard from many people - just tough it out through the pain + don't rush or worry about a schedule.

But, of course, nobody wants to be in pain all the time. And there's also an idea that ones body might adapt to hiking every day pretty well. Its not like the RSI you get from typing too much. You get blisters and they callous up. You have a knee issue and you do some stretches and try a brace for a while and read 'born to run' and learn to walk better and your muscles and tendons will grow into it.

Other stuff I'm thinking about: feel like we are going through some hazing ritual. Blisters and all. Also a trail FULL of poison oak this morning. It will be a real miracle if we don't both break out in rashes tomorrow.

And I'm tiiiired. As we have been picking up the pace, finding it harder to keep up with journaling etc. (And obviously posting on here ;))

[caption id="" align="alignnone" width="500" caption="Creek crossing near the end of the day."][![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_082006.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_082006.jpg)[/caption]

[caption id="" align="alignnone" width="500" caption="Water! And more!"][![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_153539.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_153539.jpg)[/caption]

[caption id="" align="alignnone" width="500" caption="One of the signs pointing to mikes place"][![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_145117.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_145117.jpg)[/caption]

[caption id="" align="alignnone" width="500" caption="Paradise cafe. Within a days walk!"][![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_154804.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_154804.jpg)[/caption]

[caption id="" align="alignnone" width="500" caption="Old water tanks."][![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_153727.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_153727.jpg)[/caption]

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_124329.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_124329.jpg)

[caption id="" align="alignnone" width="500" caption="Morning walk"][![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_191626.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130502_191626.jpg)[/caption]
