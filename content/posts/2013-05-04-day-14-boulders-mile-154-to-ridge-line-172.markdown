---
author: ellie
comments: true
date: 2013-05-04
layout: post
slug: day-14-boulders-mile-154-to-ridge-line-172
title: Day 14 - boulders ~mile 154 to ridge line ~172
wordpress_id: 2125
tags:
- PCT
---

Ran into lots of friends on our hike today: dragon fly and beaker, robin and Rachel (who we only caught up to because they opted to resupply in idyllwild via the paradise cafe highway. We are going to hike on to a side trail and do a few extra miles into town, but in exchange shorten our next resupply carry), [pukey and voodoo cheese](http://swamptosierras.tumblr.com).
<!-- more -->Although we aren't hiking at the same pace as anyone else, its nice to know people both a little in front and a little behind us now. So as we all take different town stops, etc, we still run into lots of familiar faces.

Much of the water today was pretty far off the trail - half mile or more trips down into canyons and such. We carried a bit more than some people to avoid _most_ of these stops but luke did make the trek down to Cedar Springs around lunchtime. He reports that it was super pretty and there were magical hummingbirds etc.. But I was happy to have the break for my little feet.

While hanging out at the trail junction, I met several day hikers who were heading into the spring from a side trail that apparently goes down to a nearby parking lot. It was interesting talking to them and a reminder of how different the thru hike and day hike experience is.

With the other thru hikers we had been talking all day about the various water treks. Armed with our water reports printed from kickoff or on the PCTHYOH app, we discussed at each aide trail crossing which spring to fill up at and which to skip. Which are running well and which aren't. How far is it between reliable sources. How much water do we have left. Do we have to stop here or can we make it to the next one? How much to fill up when we do stop?

These day hikers, on the one hand saw the spring as the destination! (Not an annoyance!) And they also had NO IDEA if it actually had any water! Much of this knowledge infrastructure is totally invisible to non-thru-hikers. There are even a few thru hikers that we run into who still don't know about it. (We learned at kickoff last year. Well advertised on PCT-L, the FB group, the PCTA twitter feed. So maybe these folks skipped KO and hate technology?) Though I imagine that won't be the case much longer up the trail. You learn pretty quick out here.

On the hazing front, amazingly neither of us seem to have any poison oak!!!!

Our first real climb today up into the San Jacinto wilderness. Rewarded by cold and crazy wind! In the last hour or so of hiking the wind almost blew me over more than once! VERY thankful it was coming up the mountain. And not trying to blow me off the cliff sides.

We stumbled upon a surprisingly sheltered ridge line site right before sunset and most enjoyed the view down into the desert while we set up camp and had dinner. The lights of palm springs shine so brightly. Reminds me of looking down into the valleys off the smokies at night.

[caption id="" align="alignnone" width="500" caption="Campsite"][![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130504_185135.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130504_185135.jpg)[/caption]
