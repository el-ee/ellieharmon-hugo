---
author: ellie
comments: true
date: 2013-04-22
layout: post
slug: day-2-rd009-to-lake-morena
title: 'Day 2: RD009 to Lake Morena'
wordpress_id: 2014
tags:
- PCT
---

Woken this morning by oppressive sunshine at SEVEN am. Maybe we won't be late starting hikers on this trip after all. Charged up the solar panel while eating breakfast and headed out for the hot hike to Lake Morena before too late.

[![image](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-IMG_20130422_084638.jpg)](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-IMG_20130422_084638.jpg)

<!-- more -->

Thought a lot about immigrants and borders today. A couple signs warning about the dangers of the desert (?I guess?) written only in Spanish near some major power lines headed down to Hauser "creek" (no water there).

[![image](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-IMG_20130422_120044.jpg)](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-IMG_20130422_120044.jpg)

And the bullet/bb holes/dents. How distressing. These clearly aren't signs for hikers. Who are assumed to be already knowledgeable and prepared for the area? Thinking about how different bodies encounter this same stretch of desert ten or less miles (as the crow flies) from the double fence -- barbed wire inside a road along a metal fence. We drove up on this road on the wrong side of the barbed wire from the starting monument the other day. What a confusing place. And a place where even though we weren't doing anything illegal felt so surveilled and weirdly in the wrong. Somebody told me the border patrol knows where every thru hiker is for the first thirty miles. Gives us all a number. I'm not so sure if this is for real but it's a little creepy and at the same time somewhat believable. Someone else said they ran into an officer hanging out up in a tree watching people. Should have taken more pictures there of the weirdness.

Anyhow.. The hikers encounter this space as a start of a long journey. Prepared with maps and smartphone apps and water reports.  A much talked about hellish hike out of Hauser 'canyon' that is seemingly always dry at the bottom this time of year. The PCTA site had some post recently about the first day being the hardest day of the trail or something. So then the triumph of this climb and the descent to lake morena campground where there is a store with ice cream and cold drinks, a campground where you will meet other hikers, and a place where you might return year after year for a reunion with your hiking class at ADZPCTKO.

And the undocumented immigrant.. Less prepared? Trying to get to where exactly? With water? With knowledge of where water is and isn't? (Un)Welcomed by signs in Spanish with bullet holes?

[![image](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-Screenshot_2013-04-21-18-57-41.png)](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-Screenshot_2013-04-21-18-57-41.png)

My smartphone app was also careful to point out that undocumented immigrants set a wildfire near the area where we camped. What kind of dire situation would prompt you to set a signal fire to attract the attention of authorities of a government that does not want you on their property? Why note that detail? To make a point that it's not always hikers' fault when a wildfire starts? To suggest that this area is inhospitable to humans and one should be careful and prepared to encounter it?

And the workers we saw near the big power line crossing above Hauser, digging out the gravel roadways in the hot afternoon sun. Their appearance would lead me to assume they spoke Spanish and were possibly from somewhere south of the nearby border themselves. How do they feel coming across the signage?

And the rodents that live in the little holes along the edges of the trail - either half caved in from the passing of hikers feet and trekking poles, or obviously very recently cleared away and doors reopened. Why there?

And the snakes that seem pleased to have a clearing where they can soak up the sun (and in proximity to all the homes of their prey?)

And the ants that march in what sometimes look to be swarms under my feet. Their homes often right in the middle of the pathway.

And the pot farmers who my water report warns me about - at some places I should stay close to the trail or I might stumble on something "dangerous" and at other places I shouldn't drink the water because it might be contaminated with bad-for-my-body fertilizers. How do they see this desert? As rent and mortgage free land on which to build a business? Is it more important that it is secluded? If the border patrol is so good at counting hikers how is it so difficult to know where all the illegal pot farms on federal forest land are? Hell my water report claims to know where they might be...how does that arrangement work?

ANYHOW. Enough rambling...

We got into lake morena nice and early and picked up ice cream, beer, chips, sour cream and onion dip at the little convenient store about half a mile up the road and enjoyed a relaxing afternoon at the camp ground.

[![image](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-IMG_20130422_153527.jpg)](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-IMG_20130422_153527.jpg)

A pair of bluebirds stopped by while we were eating chips and onion dip in the shade. Thought of G&P and our many picnics in black rock over the years :)

Finally met the disco pickle folks in person who I've been following on instagram for a while now. They had a good fire going and a crew of hikers stopped by through the night. When we woke up in the morning there must have been at least twenty or so folks in the back corner of the campground.
