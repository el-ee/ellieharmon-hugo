---
author: ellie
comments: true
date: 2013-05-22
layout: post
slug: day-32-zero-in-agua-dulce
title: 'Day 32: Zero in agua dulce [Version 1]'
wordpress_id: 2368
tags:
- PCT
---

*Note 2014-04-20: Just found this original post in moving over to Jekyll; it had been dated incorrectly in wordpress and not lost after all. Enjoy the duplication here ...*

{{< figure title="A day off sounds like a good plan :)" src="https://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_134227.jpg" >}}

We walked back over to the Saufley's this morning a little before 7am, and Donna greeted us with a big hug,  and set us up with a private room in one of the trailers on the property. We also scored a ride to breakfast (about a mile away) with folks heading back to the trail. The room in the trailer might not sound fancy to those of you at home, but it was quite a treat. Most of the hikers here stay in cots in shared tents out in the lawn, but couples get priority for the few private rooms with REAL BEDS and easy access to the REAL BATHROOM (instead of the porta potties in the lawn -- kindof nice in the middle of the night!), and it felt like an extra treat after having been at the goat farm yesterday. We felt pretty spoiled in the 'honeymoon suite' -- I can't believe I failed to take a picture!

After coming back from breakfast, we got our names on the list for a shower (which wasn't yet a very long list) and picked up some loaner clothes from the well-organized bins in the driveway. It was SO NICE to lounge around after our showers in COTTON t-shirts and comfy pants.

{{< figure title="Luke on his tablet, hanging out at the Saufley's" src="http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_105511.jpg" >}}

Our only big plans for the day involved  trip to REI for new shoes, so after signing up for a spot in the van, we spent several hours just hanging out with other hikers, and making some attempts to catch up on things like our email, and calls to our parents, and this blog, etc.

[caption id="" caption="Hikers chatting in the yard"] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_105506.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_105506.jpg)[/caption]

Lots of our friends came in today, and we enjoyed a relaxing afternoon soaking our weary feet in salt baths under the delightful shade of an amazing huge tree in the lawn (it's so rare to come across a bit of shade in all this desert hiking!). We enjoyed listening to Wildfire play guitar and Sharky's accompaniment on a small banjo.

[caption id="" caption="The Saufley's yard, turned into hiker trash heaven. Tents with cots, RVs with a shower, kitchen, and TV, porta potties, fire ring, internet station, and plenty of space for sitting, talking, eating and drinking"] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_134230.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_134230.jpg)[/caption]

The Saufley's are some pretty awesome people, who totally turn their whole garage and yard into the perfect spot for thru-hikers to stop. It's definitely called 'hiker heaven' for a reason.


[caption id="" caption="The impressively organized mailroom at Hiker Heaven"] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_142205.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_142205.jpg)[/caption]

Their garage is information and mailing central (since there is no official post office in town). The information board had everything a hiker could want to know -- addresses for mailstops further up the trail, information about trail angels who give rides to and from certain trail heads, a register to sign in and see who has checked in ahead of you, water reports recently printed for you to pick up for the next couple of sections, etc.

[caption id="" caption="The information board at Hiker Heaven"] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_142149.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_142149.jpg)[/caption]



[caption id="" caption="Information Board, another side"] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_142153.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_142153.jpg)[/caption]

And, we were also excited to see that Cuddles is playing again tonight, right here at Hiker Heaven! He is a pretty rad cello player who we first saw perform in Idyllwild, and once again the concert in the Saufley's yard did not disappoint. We really enjoyed a multi-movement piece written on a circus theme -- hope to see him again on up the trail!

[caption id="" caption="Cuddles's cello tour, Agua Dulce stop"] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_142159.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_142159.jpg)[/caption]


[caption id="" caption="The only PO in town is in this garage. Yay for USPS.com"] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_142407.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_142407.jpg)[/caption]


Although we didn't have a package to pick up here (we decided to just shop in the local grocery/market for our resupply this time), I did a little mailing of my own here -- sending a new package of dirt to Christina, and sending home some things that we decided either to replace or that we no longer need. I think at this part of the trail, many hikers are changing out their gear, and we're no different ;) Katie has been AMAZING helping us by sending out our resupply packages of food and maps, and by holding all of the gear and stuff that we've changed our minds about wanting to carry after all.

[caption id="" caption="Package for Christina"] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_141931.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_141931.jpg)[/caption]

Later in the afternoon, we loaded up in a rented van with a bunch of other hikers for a trip to the REI that is about 40 minutes away. Both of us were so very happy to buy some new shoes that were a bit bigger, and also replace our camp shoes with some really comfy Teva 'mush' flip flops.

[caption id="" caption="The van ride to REI"] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_144923.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_144923.jpg)[/caption]

The ride to REI with the other hikers was kindof a culture shock for all of us -- sitting in traffic on the freeways outside of LA, passing new suburban housing developments with signs advertising 'Live Green. Save Green.' -- which caused the van to erupt in various shouts about 'What do these people do? Commute to LA?' 'What do they collect rainwater for their landscaping?' Not much love for that lifestyle from the hiker trash... all happy to be reminded that while we may have wicked blisters on our feet, at lest we're not stuck in some 'rat race' !

[caption id="" caption="Luke rocks the loaner clothes on our REI trip"] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_152356.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_152356.jpg)[/caption]

[caption id="" caption="LA Traffic on the REI trip"] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_171110.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_171110.jpg)[/caption]

Of course, we do appreciate our high-calorie fast food out here ;) And In-N-Out is a rather more loved piece of civilization. I can't say I'm big on the burgers myself, but I killed some animal style fries and a shake The out-of-state folks were pretty amused by the calorie count labels on the menu -- which makes you a little freaked out that 'normal' people actually eat here, but as an always-hungry hiker makes for a good competition about who can eat the most (for the least $$).

[caption id="" caption="In-N-Out Stop!"] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_184114.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_184114.jpg)[/caption]

[caption id="" caption="All are pleased with their In-N-Out purchases!"] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_185042.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_185042.jpg)[/caption]

Other topics of conversation: the big ice axe question as we all start to anticipate the Sierra. Given the incredibly low snow that CA has received this winter, Luke and I have decided not to bring axes or microspikes and I would say about 90% of people are doing the same. But, there are a few (mostly swayed by overly-cautious listserv advice) who are planning to mail one ahead to themselves at Kennedy Meadows -- the mentally big (but geographically tiny) place that marks the 'beginning' of the Sierra, and 'end' of the desert.
