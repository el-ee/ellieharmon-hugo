---
author: ellie
comments: true
date: 2013-05-19
layout: post
slug: day-29-one-ridgeline-to-another
title: 'Day 29: One ridgeline to another'
wordpress_id: 2312
tags:
- PCT
---

Today was a pretty long day for us. We ended up walking 24 miles, and I slackpacked 2 more than that to return for my phone which I left on a picnic table midway through the day :/


[caption id="" align="alignnone" width="500" caption="Road walking on the detour"][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130519_085949.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130519_085949.jpg)[/caption]

In this part of California, Highway 2 (the '[Angeles Crest Highway](http://en.wikipedia.org/wiki/Angeles_Crest_Highway)') has the best cut through the mountains. So, naturally, on the trail, we crisscrossed it back and forth at least 9 times -- literally. (The GPS app we use names all the road crossings as waypoints, and when we cross a road multiple times, it shows up like Hwy2a, Hwy2b... this time it went all the way to Hwy2i.) I guess this is kind of like the SoCal version of the BlueRidge parkway (which I totally road walked through the Shennies when I hiked the AT in 2008). As a hiker, it's a little frustrating to walk across the road, up a tiny hill on one side, back down the tiny hill, back across the road, down a tiny valley on the other side, back up to the road, back across the road... and on and on. So we did lots of extra elevation, had to run back and forth across it, AND got the worse views, because the road cutaway is much more dramatic than that of the trail hidden down in the brush.


<!-- more -->


[caption id="" align="alignnone" width="500" caption="Crossing the road scarier than waking on it"][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130519_134435.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130519_134435.jpg)[/caption]

BUT! The good part for us today was that we actually got to walk ON the road for a short way, thanks to a detour for [an endangered frog](http://www.mylfrog.info). This gave Luke and I some time to actually walk BESIDE eachother and have an easier time at conversation than we do walking single file through a narrow trail. Also, like the blue ridge parkway, the views from Highway 2 are spectacular, and there was very little traffic, so we didn't feel like it was particularly dangerous or anything. Also... at just about every trail junction, there were privies and picnic tables! What a civilized treat!

[caption id="" align="alignnone" width="500" caption="Detour map"][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130518_180336.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130518_180336.jpg)[/caption]

We talked a lot about the differences in political respect that a road gets versus a trail -- the endangered species detour, for example, ends exactly at the border with the road. I find it hard to believe that the frog's habitat ends so neatly at this boundary line. It seems more that cars, in our society, would never be asked to re-route around such a place. Instead, hikers -- arguably with much more to lose -- are asked to walk elsewhere. Technically, the PCTA suggests a very lengthy re-route on trails, approximately 20 miles to go around the 4-mile section of closed trail, but all but two thru-hikers we've met so far opted to take the 'old detour' on the highway, which is about 4.5 miles.

The road also cuts through the wilderness in such a different way than the trail. It controls and shapes the earth around it -- with the aid of big $$ and big machinery -- whereas the trail bends much more to the earth -- with the aid of mostly hand-tools and volunteer labor. So, the road is quite nicely sloped, and it just seems to go wherever it's going, with huge vistas out across its 30' + wide swath across the ridge. Whereas the trail is bumpy ups and downs, a narrow path, always at the verge of being overgrown, curving around this and behind that, and with views hidden by scrubby trees and bushes. You can probably tell how I was feeling about the trail today .. not so hot.

We were also wondering when the road was built, assuming it was an early 20th century event (which wikipedia suggests is at least partly true, started in the 20s, but apparently not finished until after WWII). It feels like this was a time when creating big scenic highways was THE thing to do in such areas. It's too bad that the trail can't leverage some of this work and just run alongside the road or something -- set off a 6' wide sidewalk on one side! It'd be so great!!


[caption caption="The only sign acknowledging the detour at the PCT junction is this note from a thru hiker pointing out that the right direction is behind you. And oblivious day hikers continue along the stream."] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130519_111655.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130519_111655.jpg) [/caption]


[caption id="" align="alignnone" width="500" caption="In contrast, a well signed burn area."][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130519_162337.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130519_162337.jpg)[/caption]







And, then, when we made it back to the junction with the closed section of the PCT, do you think there was some yellow tape marking the area closed? Or maybe at least a sign at the closed portion of trail 'The 4 miles south of this sign are closed to protect the Mountain Yellow Legged Frog'? Well if you thought so, you'd be wrong. In fact, there was nothing marking the closed section of trail as closed at all! And, being that the closure starts at an intersection of the PCT with a popular day hiking trail that comes out of a campground and follows a nice stream, you better believe it was PACKED with day hikers (AND THEIR DOGS!!), all totally oblivious to the whole thing. This seemed like a MAJOR information fail by the USFS to me. Yeah, there are these tiny signs up at the trailhead bulletin boards about the closure, but seriously, does anyone thing anyone reads those? They always say the same dumb things 'this is bear country' 'this is mountain lion country' 'don't feed the animals' etcetcetc. But, all the over-informed PCT hikers are walking all around in crazy ways and those who are 'purists' (who want to hike every official mile of the official trail) are (1) fretting about missing the closed section and then (2) fretting that the official detour adds 16 miles to the section -- basically a whole extra day.

At lunch, we ran into a friend of [Storytime](http://postholer.com/allan) who does trail maintenance in the area, and was out looking to meet up with his friend for the afternoon. We had a good time talking with him. He said he wasn't thru-hiking with Storytime because he 'has responsibilities' -- we heartily encouraged him to ditch them, but I'm rather aware it's not an easy proposition for many people ;)

In any case, talking with him was another reminder of the different knowledges about the trail. He kept referring to places by names that we'd never heard of! Glued to our water reports, and looking just from one water stop to the next, we rarely break out the topo maps to see the names of features nearby. For example, we just knew we camped on a ridge last night somewhere above Islip saddle. It took us quite a while to realize that this was the same place he was talking about when he said 'Mt Wiliamson'! And, since we broke up the maps in chunks that match our resupply stops rather than the official 'sections' of the trail, we hardly know what section letter we're in.


[caption id="" align="alignnone" width="500" caption="We did not miss passing the 400 mike mark though"][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130519_140602.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130519_140602.jpg)[/caption]



And to end the day, we had our first really nasty encounters with the Poodle Dog Bush. There was a little bit of the stuff in the afternoon -- and a treasure box cache of trail magic including fresh carrots which majorly improved our ramen dinner.

But then near fountain head spring, our last water source before camping, the stuff was just EVERYWHERE. Totally unavoidable except by taking paths that cut off the official trail, up and down a steep hillside... which after having hundreds of people slide around on it trying to avoid the poisonous plant is totally crumbling away. This really slowed us down for our last few miles of the day, and we had a hard time finding land that was both flat and poodle-dog-free, but we finally settled into camp just as it was getting dark. On the bright side (hah) there was quite a large moon tonight (maybe 3/4 full?) and we enjoyed the shadows it cast. We finally ended up with great views down into the valley below us just as the towns lit up for the evening.

As a result of all this stuff today, we are planning (and looking forward to) some more road walking tomorrow -- optional this time, around the old Station Fire burn area in order to avoid what is described as not so easily avoidable poodle dog bush. In our water report (which serves as real-time trail info for many more things than just water), the growth we walked through today was described as 'mostly avoidable' or something, so we don't have any desire to experience poodle dog bush that is *more* overgrown than that. It will be a miracle if neither of us breaks out in a horrible rash sometime in the next few days...
