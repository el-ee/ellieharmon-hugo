---
author: ellie
comments: true
date: 2013-05-13
layout: post
slug: day-23-big-bear-to-little-bear-springs-horse-camp
title: 'Day 23: Big Bear to Little Bear Springs Horse Camp'
wordpress_id: 2233
tags:
- PCT
---

Big bear to little bear today! How fitting!

[caption id="attachment_2239" align="aligncenter" width="525"][![Looking back across the lake at Big Bear](http://ellieharmon.com/wp-content/uploads/IMG_20130513_153753-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130513_153753.jpg) Looking back across the lake at Big Bear[/caption]

<!-- more -->

Didn't make it out of town until ~11. Had to fit in a second breakfast at the Teddy Bear Cafe -- french toast with boysenberry syrup -- yum!! And got stuck around the hostel chatting with folks a bit, too.

[caption id="attachment_2235" align="aligncenter" width="525"][!["Well, here in Big Bear we call this Dump Road"](http://ellieharmon.com/wp-content/uploads/IMG_20130513_113434-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130513_113434.jpg) "Well, here in Big Bear we call this Dump Road"[/caption]

Once we got going, we quickly came to a road, which had a surprising amount of traffic on it, and a giant bald hill in the background. A pickup driver stopped his car to say Hi and see if we needed a ride into town. No, we said we were just leaving, but asked what road it was we were crossing, and what is that, pointing back at the hill. "well, here in Big Bear, we call that The Dump. And we call this Dump Road." DUHHHH. The giant scraped clean hill is obviously a pile of trash! Hahaha. We met someone later on the trail who was similarly perplexed by this large unexpected mountain, but thought it was really pretty and had stopped for several pictures. They were a little disappointed to learn it was the dump :)

[caption id="attachment_2236" align="aligncenter" width="525"][![Flowers on the trail today -- I felt like we were hiking through popcorn!](http://ellieharmon.com/wp-content/uploads/IMG_20130513_122214-768x1024.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130513_122214.jpg) Flowers on the trail today -- I felt like we were hiking through popcorn![/caption]

[caption id="attachment_2237" align="aligncenter" width="525"][![The trail wound up a mountain on switchbacks that kept crossing over these patches of scree. The ground beneath my feet is always changing so fast! A few yards of red dirt, a few yards of scree, a few yards of black dirt. And on and on.](http://ellieharmon.com/wp-content/uploads/IMG_20130513_122246-768x1024.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130513_122246.jpg) The trail wound up a mountain on switchbacks that kept crossing over these patches of scree. The ground beneath my feet is always changing so fast! A few yards of red dirt, a few yards of scree, a few yards of black dirt. And on and on.[/caption]

[caption id="attachment_2240" align="aligncenter" width="525"][![Grass on the ground, dead trees in the air?!](http://ellieharmon.com/wp-content/uploads/IMG_20130513_160958-768x1024.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130513_160958.jpg) Grass on the ground, dead trees in the air?![/caption]

Our late start ended well. We arrived before dark at a great campsite about 20 miles in, with a picnic table and outhouse and everything! Outhouse was even complete with a Maxim from 2008 ;)

And, our friend Sour Cream was hanging out at the picnic table -- someone we ran into back in the coffee shop in Idyllwild. He's a pretty interesting guy from Ottawa (IIRC), who has to be one of the happiest people on the trail. Every time we run into him, he always greets us with a great smile, and an enthusiastic recount of his day's hike. A couple from Corvallis -- Shark Bite & Faucet -- arrived a little bit later. And, yes, they do know Camille's mom from the farmer's market!

[caption id="attachment_2241" align="aligncenter" width="525"][![Luke in the burn](http://ellieharmon.com/wp-content/uploads/IMG_20130513_173501-768x1024.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130513_173501.jpg) Luke in the burn[/caption]

[caption id="attachment_2242" align="aligncenter" width="525"][![The burn area](http://ellieharmon.com/wp-content/uploads/IMG_20130513_173512-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130513_173512.jpg) The burn area[/caption]

Right before the campsite, we walked through a long stretch of trail through our first big burn area. Areas of devastation like that are always interesting to me to see. Reminded me a lot of the superfund site near Palmerton, PA that I walked through on the AT.

[![palmerton superfund site](http://farm4.staticflickr.com/3157/2914980751_5512954dfb.jpg)](http://www.flickr.com/photos/_ellie_/2914980751/)

In this case, it's clearly been several years since the burn, and life is coming back - plants and lizards and birds. The earth isn't /going/ anywhere if we don't 'take care' of it. But, we (humans) might end up going out with a good bang. Like this area is clearly going to be a very different forest now. And, like the Palmerton site is supposedly still quite poisonous to people (don't drink the water; don't eat the blueberries), many of the burn areas out here are now home to the infamous, poisonous poodle dog bush. None at this spot... but I'm sure there will be more on that later!

Not sure if I wrote about this on the blog yet or not? But, have been thinking a lot about numbers lately -- there  is a lot of counting that happens out here, in particular miles, calories, and ounces.

Back in Idyllwild, Luke had to sew pleats into his pants because he's shrinking (ohno!), and got several recommendations to buy some fruit hand pies at the store for their great calorie:price ratio (500:50cents), and a pretty quality calorie:weight ratio as well (I dunno).

And people thinking of buying new gear, are often caught in a comfort:weight ratio debate. Like, we really enjoy our therma-rest neo-air sleeping pads, which weigh a little more than the thermarest zlites which most people carry, but to us, the night time comfort is worth the weight! The funny thing about this comfort:weight calculation is that the weight part is really about comfort, too -- comfort while hiking, comfort of your shoulders, comfort of your feet, comfort of your knees...

Then there's the mileage thing. It seems that everyone out here refers to things by the milepoint. Like a conversation might go like, "Hey where are you headed tonight?" // "That creek at mile 245." I guess this is because everyone is using halfmile's maps and/or app. And, every waypoint has a mileage number. A road that crosses the trail around mile nine would be RD009, or a stream that crosses at mile 245 would be WR245. But, even though I have those maps and use that same app, I just can't get used to referring to things by their milepoint. What sticks with me is usually the creek name or road name; or how far away it is from my current location. But I'm not terribly good at knowing my current location by its milepoint either, so I can't even do the math to figure out what other people are talking about. Have to pull out my app and translate to make sense of anything.
