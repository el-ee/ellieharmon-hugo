---
author: ellie
comments: true
date: 2013-04-23
layout: post
slug: day-3-lake-morena-to-cibbets-flats
title: 'Day 3: Lake Morena to Cibbets Flats'
wordpress_id: 2023
tags:
- PCT
---

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130423_060258.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130423_060258.jpg)

Much more pleasant morning at lake morena today not in the blistering sun. Got up a little after six anyway ;)

<!-- more -->

Did some interviewing for my research project this morning at the campground, so didn't head out for hiking until ten or so.

Similar landscape today. A few more grassy and tree areas in between the desert scapes.

Saw lots of snakes! Caught the last half of a rattler slithering off (silently), a couple seemingly harmless snakes (one literally ran into lukes foot from the side of the trail, trying to cross between us!), and one dead snake with a half eaten lizard (about its same size) in its mouth/body. Bit off a bit too much I guess.

Nice hike with a shady break at some picnic tables near I-8 for lunch.

On that note, so far I've been surprised about how many roads there are out here. The trail feels both more and less remote than the AT. More nearby roads if possibly fewer crossings. Less towns that the trail actually passes through, but (fitting for my other SoCal experiences) plenty of people driving all over the place.

When I back, I'd be curious to do some map work...how many miles of trail are within _x_ miles of a road? Does the SoCal PCT just seem closer to roads because the lack of trees and relative flatness make it easier to see and hear them?

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130423_120725.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130423_120725.jpg)

And along with the roads. Lots of power lines. Entertained myself with pictures of lots of signs and barbed wire today (only a few made it on the phone. A difficulty of remote blogging is getting pictures OFF a regular camera is so much extra work!).

There are a lot of apparently important borders and boundaries out here. I would think the barbed wire would just be used when passing between different livestock areas but it seemed to be _everywhere_! Is 90% of this trail cattle land?

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130423_162423.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130423_162423.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130423_163124.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130423_163124.jpg)

The road and accompanying sign heading down to the campground where we stayed. Chosen as much for the availability of water as anything else. Though the picnic table was nice :)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130423_174232.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130423_174232.jpg)

And this lovely picture of the salt ridges on lukes shirt! It's hot out here! Don't worry though we are drinking plenty of nuun and eating salty chips etc along with our water :)
