---
author: ellie
comments: true
date: 2013-05-20
layout: post
slug: day-30-ridgeline-to-north-fork-ranger-station
title: 'Day 30: ridgeline to north fork ranger station'
wordpress_id: 2315
tags:
- PCT
---

We recorded our 4-week movie for you guys while we ate breakfast this morning -- some super rad cranberry walnut bread that we packed out from the farmers market in Wrightwood. We're a few days late, but will try to get it up as soon as we can.

Themes of the day: Poodle dog bush & FIRE!

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130520_184025.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130520_184025.jpg)


<!-- more -->

We did lots of road walking, and if I went back and ever did this section again, I'd do even MORE of it! There was nothing quite as bad as last night in the sections we walked through, but it was completely mentally exhausting. The trail was completely overgrown in many areas -- so much so that you literally couldn't see the trail -- and a few stray poodle dog bushes mixed in with the other growth. So, while the PDB was easily avoidable, it was really taxing on the senses to spot it so you knew what to avoid. My eyes and nose were working overtime the whole part of the day that we spent actually on any 'real' trail; it was so frustrating to smell it and know it was close, and then having to stop and work really hard to see it in the middle of lots of other greenery.

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130520_141500.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130520_141500.jpg)

We skipped what was described as the worst part of it by walking on Mt Gleason road for several miles -- an old road that is currently closed to traffic and, unbeknownst to us when we chose to walk it, goes through 'Camp 16' the 'station' for which [the station fire](http://en.wikipedia.org/wiki/2009_California_wildfires#Los_Angeles_County) is named.

The first thing we came upon was a really amazing and sobering memorial to [two firefighters](http://witnessla.com/fire/2009/admin/firefighters-arnie-quinones-ted-hall-a-hero-story/) from Camp 16 who died in the fire -- a journal for each one with a note asking passers by to leave some thoughts. While somewhat sad, it was also quite touching, and nice to read some thoughtful and caring comments from both strangers coming by over the years, and our fellow hikers who usually just sign their name and a date in the regular PCT registers.

Then we actually walked the road through the burned remnants of the station. We didn't spend too much time exploring, but there is something intriguing about seeing the ruins of recent civilization. Felt a bit like walking through the old ruins of crumbling centuries-old buildings in Rome, but everything was more familiar. Basketball hoops, and picnic tables and grills. We found out later this was a [prison camp](http://www.fire.lacounty.gov/airwildland/airwildlandfirecamps.asp) -- which I had no idea existed at all! But, nonetheless, it looked like the firefighters stationed up there had some good times at the top of the ridge -- a really beautiful location.

The road also took us up nearly to the top of Mt. Gleason which we would have just walked around on the trail, and we enjoyed the off-trail walk again today, for many of the same reasons as yesterday:

1. Walking side by side and TALKING to eachother!
2. Being able to LOOK at the scenery instead of just trying to watch every single footstep
3. No bullshit ups and downs, just smooth mountainside climbs and lovely ridgeline walks. The road really makes the mountain bend to it. Whereas the trail bends much more to the mountains!
4. And, of course, having 10' to avoid the PDB was much more pleasant than dancing around with only 10" of space.


[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130520_182518.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130520_182518.jpg)


The views were lovely all day. In the fire areas, lots of new life is taking over the landscape amidst the charred skeletons of the old trees. Even the PDB is quite beautiful when it's in bloom and you're not stressed about avoiding it. We stumbled upon one particularly amazing meadow full of multi-colored blooms right near the edge of the burn area.

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_070856.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_070856.jpg)


[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_064609.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_064609.jpg)


We made it down to the North Fork Ranger Station where we camped for the night before too late. The sole ranger stationed out there these days lovingly maintains a water cache for thru-hikers on a picnic table hidden back behind a GIANT propane tank. And, although it's technically a 'picnic' and 'day use' area, there are 2 suspiciously tent-pad-like flat gravel beds where we set up for the night. The weirdest part about this place, besides being a seemingly off-the-record/look-the-other-way campsite for thru-hikers, is that it is RIGHT under some HUGE high voltage power lines. I swear I can FEEL the crackle in my arm muscles... but that might just be all in my head.

We've gotten a lot of help from rangers and fire stations -- earlier in the day we filled up our water from the faucets at another ranger/fire station. It would be a really hard hike through this area without this kind of help. I wonder where the water comes from though? And why to help with thru-hikers? This picnic area at North Fork, for example, has no water available for other passers-by. At many of the campgrounds we went through today, the water pipes were permanently capped or turned off. Is this because it is too expensive to truck in water to fill the tanks that used to supply them?
