---
author: ellie
comments: true
date: 2013-05-01
layout: post
slug: day-11-a-ridgeline-somehwere-to-agua-caliente-creek
title: 'Day 11: A ridgeline somehwere to Agua Caliente Creek'
wordpress_id: 2088
tags:
- PCT
---

We reached the 100 mile marker today!! There is a movie of me dancing that will appear sometime in the future...

No pictures because the phone was dead most of the day and I neglected to take any in the evening. Sorry!

<!-- more -->

In any case, today was another day of lovely strangers making the trail a nice place to be.

We left camp a bit late, around 8:20 after having been up much of the night thanks to the wind. But, that worked out pretty well in the end, we got to Barrel Springs at a reasonable time for lunch, enjoyed the shade and some celebratory 100-mile sodas someone had left behind there... and then.. just as we were getting ready to hit the trail again to Warner Springs, along comes a guy hanging up signs inviting hikers over to his nearby RV where he, his wife, and another couple were hanging out doing trail magic!

So, we sat down in the shade, and enjoyed an ICY cold beer (seriously, there were pieces of ice stuck to my beer can! AMAZING!). The angels who run hostels and stock water caches are truly wonderful people who make this trail possible. But this kind of unexpected magic is so very delightful.

We skipped out on the promised 4pm chili dogs and instead took off around 12:20 booking it to warner springs in right at three hours -- including a visit to the cool eagle rock! Managed to pick up our package at the PO before it closed at four (thanks to the lovely retired ladies who skipped their regular monthly casino trip to hang out by the trail shuttling hikers around! Because its 'more important' one lady told me! I'm not sure I'm that important but I really appreciated the favor!!)

We charged up our phone and spent a few bucks on ice cream and Gatorade at the awesome temporary hiker store set up at the local community center (which is amazingly RIGHT on the trail) and then got back to it putting in a few more miles, setting up camp right around sunset again. The late afternoon/evening hiking is SO nice in the desert :)

Second twenty mile day this week and I was exhausted at the end of it. And despite having a shower just the other day, feeling really dirty. The wind last night blew tons of very fine dirt through the mesh on our tent and everything, including me, is just covered in it.

Some frustration today: I did pickup a blister on my left foot between my big toe and first toe...all because I didn't stop to take a rock out of my shoe. /sigh put a blister bandaid on it at lunch which moved around on the hike to warner creating an even bigger blister! A ah!
