---
author: ellie
comments: true
date: 2013-04-10 07:32:44+00:00
layout: post
slug: american-culture
title: American work culture
wordpress_id: 1930
---

Had a good lunch with Mel the other week. Was thinking about something she said on my walk home today.. Might my dissertation end up being a kind of critique of american culture? I hope so.

<!-- more -->

I've never felt terribly comfortable here/anywhere. From being the teachers kid who lived far away and was poorer than everyone else in elementary school ... to being the opinionated liberal affluent kid at comparatively poor conservative southern baptist middle and high schools ... to the girl in a computer science department ... to the computer science grad working as a high end furniture and textile designer at a Santa fe shop ... To my latest series of academic identity crises at Irvine

And throughout -- Maybe most strongly influenced by my time in middle school and high school when my politics were so anti mainstream -- always encouraged to question and argue (maybe most strongly by my grandma). So, in any case, I approach life -- and how I'm being told to live it -- with a lot of skepticism. Maybe an abnormal amount.

And as much as I harass Luke about expecting too much consistency from other people (we are human not machines after all)... It's the inconsistencies i perceive in others that always stand out to me. Like after a day that included a lot of time spent talking about how schedules get pushed around and people (in positions of lesser power) are expected to deal with last minute changes... That's when I get a text at 8pm asking to move a 9am meeting from one of those co researchers.

I don't see it as terribly annoying to get the text. Or that that person has done something annoying. Actually it's great because now I can get coffee with someone else tomorrow which I'm quite pleased about. But.. I am struck by the fact that both of us were working at 8pm at night. (Well I was walking home from the lab, but close enough). And that neither of us have that much control over the whole thing. And the technology facilitates, or plays along might be a better word..the technology is complicit with us. On my body wherever I am. The 8pm communication about a 9am meeting is _possible_.

But also there's more work than can be done in a reasonable 'workday' .. And we're both carrying on. Maybe a bit frustrated but nonetheless doing it. Worrying that we're not doing enough, in fact. Today was spent adding more tasks to the list. And its hard to talk in this kind of space about critique.

And that's the bit about culture (and affect) that seems most interesting and pressing to me at the moment. That the critique step away from not just  technologies, but even/also social groups, institutions, and organizations. And talk about culture and stories and beliefs and expectations and desires.

And how do we not just manage it all but also (occasionally) push it all away. I am hoping that some of the other hikers that I hope to meet this summer can share with me some insights into these questions this summer. Clearly I don't know. The way I've wrapped my hike into my work both making it logistically possible (as part of a life that fits into a PhD timeline) but also making it impossible as a sabbatical or disconnection of my own (it will still be work, at least some of the time).

How do you disconnect from jobs pressures families expectations. How do you take yourself out of society for a bit. What preparations are needed. What enables it. Will. Money. Desire. Support. How does that skipping out land you skipping into some other community of fellow hikers, organizations like the PCTA, trail angels. Is the existence of this other community required first?

