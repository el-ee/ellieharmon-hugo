---
author: ellie
comments: true
date: 2013-03-19 03:32:37+00:00
layout: post
slug: a-confession-i-read-the-conclusion-first
title: 'A confession: I read the conclusion first. '
wordpress_id: 1891
tags:
- 1k plateaus
- deleuze &amp; guattari
- ontologies
- reading
---

Like playing the last track of a great rock opera first, I realize this means I've read into the conclusion nothing but my self. It can only speak to what's come before (my life/ experience/ current conversations on my mind) and perhaps it was written instead as the culmination of a _composition_ ... a whole that I haven't listened to, not even in part.

<!-- more -->

But the introduction said, read this in any order you like. Treat it like a record... "except the conclusion, which should be read at the end." So, really, how could I _not_ read the conclusion first?! Even if I were into direction-following, then I would be led to think that the conclusion should never be read. I mean, when is "the end" of listening to an album? And, we can't have that!

But, maybe this is why I said this was a _confession_ to make and not a defense. In today's age of YouTube sensations, single-song mp3 downloads, and algorithmic-recommendation-based 'radio' (-a) stations, I have lost all respect for an album as a coherent entity. But, I remember listening to Pink Floyd and Who albums in college, and going to see all variety of 'jam bands' and DJs that put on a show as a _show_ (who buys albums? much less _singles_ -- blasphemy! -- it's all about the cohesiveness/ emotion/ coherence/ experience/ event of the live show as a whole _thing_). So that was about having respect, then, for a body of work as a _composition_ (a "unity" "in spite of the diversity in its organization and development")... and perhaps I haven't brought that respect to this reading (or anything else in my life, as of late).

In any case, though, reading _myself_ into this conclusion, I'm struck by the way Massumi's remix of D&G* seems to speak to my own uneasy-ness with the 'ontological turn' as someone described the latest fad these days -- the latest fad which we might hold this 30+ year old book partially responsible for, too.

At the end of the first section of the conclusion (S [for] Strata, Stratification -- what is this? A book for pre-schoolers learning their letters?) Massumi translates:



> 
  For **outside the strata or in the absence of strata we no longer have forms or substances, organization or development, content or expression**. We are disarticulated; we no longer even seem to be sustained by rhythms. How could unformed matter, anorganic [inorganic?] life, nonhuman becoming be anything but chaos pure and simple? Every undertaking of destratification (for example, going beyond the organism, plunging into a becoming) must therefore observe concrete rules of extreme caution: a too-sudden destratification may be suicidal, or turn cancerous. In other words, **it will sometimes end in chaos, the void and destruction, and sometimes lock us back into the strata, which become more rigid still, losing their degrees of diversity, differentiation, and mobility**. [my emphasis]




To back up a little, "strata are phenomena of thickening on the Body of the earth...accumulations, coagulations, sedimentations, foldings." Reading this through my own life/ history/ experience/ knowledge, (rather than whatever is in this book) I am connecting this idea of strata with something like Barad's or Haraway's materialsemiotic/materialdiscursive/naturalcultural _stuff/matter_ of the world. Barad even uses the sedimentation metaphor herself. How could one ever go "outside the strata?"

I think what I like most about these feminist STS readings of the matter of the world, is that there is a deep recognition of the inseparability of humanity from stuff in science. They might not put it quite this way, but, to me, the great difficulty (impossibility/arrogance) of a shit to ontology (from epistemology) is that it presumes the possibility of (e)vacuating the human/self from the [social] science/philosophy.

How can we know beyond knowing? M/D&G seem keen on doing such work, and yet, call to the fore the point that de-stratification results in a vacuation of content and expression. So, what is up with M/D&G? What kind of content-less, expression-less content and expression are they looking for academics to produce?

And, more generally, what is up with the 'ontological turn'? Don't we really mean something much more simple (and much more modest) along the lines of: what I think I know, might not be the one true truth at a level beyond/above/greater than knowledge because I, in my situatedness, my humanness, my very existence, my thinking this thought about thinking am in no position to judge right from wrong beyond my own experience/ thinking/ knowledge? And wouldn't a more radical position of modesty might be a return to epistemology? Why  claim/ argue that there are multiple realities? What does that even mean when spoken from a position of limited/ constrained/ confined/ located/ real/ human/ alive/ orgianic knowing.

Yes, we can make moral and practical claims; and, yes we can argue about the basis of those claims on better and worse kinds of evidence. Where better and worse might have complicated metrics. But those metrics are never "Reality" with a capital R or "Being" with a capital B. Those metrics are always _my knowledge/experience of_ my being/others beings. My knowledge/ perception/ emotion/ feeling/ sense/ intuition/ educated guess/ rationalized inquiry into possible realities.

Seriously. Lost on the ontology thing. Happy that M/D&G might be used to say it's crazy, too. But confused that they also seem to imply that we should all be going after it.

And now to read some other selections from this book... perhaps therein lies an answer or two...

* And since I'm digging this album metaphor, it seems like I should just run with it and put Massumi first here. Because the translation from French to English is surely more transformative than most DJ remixes.

