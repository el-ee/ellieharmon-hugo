---
author: ellie
comments: true
date: 2014-04-17
layout: post
title: '2013-05-24 | Day 34: to the Andersons'
tags:
- PCT
---


[![water cache!](https://farm4.staticflickr.com/3710/11136511105_37338b643a_c.jpg)](https://www.flickr.com/photos/_ellie_/11136511105)

Nice, relatively short walk today. Started out the morning with a brief stop by a mini-water cache maintained by the Andersons. We didn't end up taking any water as we had plenty, but we stopped to say hi to Wild Child & Seminole who seem to be doing well on their travels today. Guess they didn't make it a whole lot farther than us last night :)

[![Oasis Cache](https://farm6.staticflickr.com/5511/11136674653_6b627a11f0_c.jpg)](https://www.flickr.com/photos/_ellie_/11136674653)

A couple of hours later, and in perfect time for a morning break, we hit the more fancy Oasis cache, also maintained by the Andersons. Sadly, all the beer was gone, but we SOOO enjoyed the chairs tucked away in a shady little alcove.

We ran into Scones & Doodles, who have been trying to night hike the desert to avoid the heat. They were rolling in at 10am, hoping to go to sleep. They briefly said hello, and then headed over to a shady spot across the trail, where they hoped it would be quieter than at the cache, where presumably people would be coming and going and hanging out all day.

Overall, I feel like we've been pretty lucky in the desert weather-wise. It hasn't been nearly as hot as I was expecting, and, at least in this stretch, there are places like this where one can get a shady break in the trees/bushes. We've mostly been just powering through the daytime, and no regrets (yet). I think it'd be a lot harder to try to change my sleep schedule to do the whole night-hiking thing. I am not good at making my body adjust quickly like that.


[![a painting about the desert, oasis cache decor](https://farm8.staticflickr.com/7401/11136532476_89c5313285_c.jpg)](https://www.flickr.com/photos/_ellie_/11136532476)

I guess this picture is about how we *should* feel coming into this cache. Scary!

[![Coconut M&Ms](https://farm4.staticflickr.com/3834/11136684943_b9483f4bbd_c.jpg)](https://www.flickr.com/photos/_ellie_/11136684943)

I had a pack of my coconut m&ms for a snack here -- seemed to fit the theme so well! (Not sure if you can tell from the above pic, but the Andersons are famed for their Hawaiian theme'd everything. Shirts for hikers at their house, and the chairs at the cache here were similarly decorated.)

One last picture from the cache:

[![was finally feeling like we were making progress when i saw this sign, and then thought, oh, we still have a WHOLE APPALACHIAN TRAIL worth of miles left to go](https://farm8.staticflickr.com/7446/11136506115_b6610d3b72_c.jpg)](https://www.flickr.com/photos/_ellie_/11136506115)

After having been out here a full month now, and having survived a significant 400 miles of walking, and having made it to the so so famous Saufley's house -- which at the beginning of the trail felt like just about as far away as the end -- I was just starting to feel like we were making REAL PROGRESS on this walking thing. And, then, this other cache decoration gave me a little reality check -- we still have A WHOLE APPALACHIAN TRAIL worth of miles left to go! Yikes! It made me feel like our actual progress was closer to 0 miles.

The rest of the hike went okay today. Some sights from the desert:

[![Manzanita bark](https://farm8.staticflickr.com/7372/11136659603_ece5e2c600_c.jpg)](https://www.flickr.com/photos/_ellie_/11136659603/)

[![Manzanita Bark](https://farm4.staticflickr.com/3711/11136549734_5913c78952_c.jpg)](https://www.flickr.com/photos/_ellie_/11136549734" title="more bark by ellie, on Flickr)

I stopped to take some pictures of the manzanitas with their lovely curling bark -- and a couple even came out on the phone!


[![Signage](https://farm6.staticflickr.com/5478/11136475565_d89e8dca1d_c.jpg)](https://www.flickr.com/photos/_ellie_/11136475565)

As usual, crossing plenty of roads and other trails. I found the level of signage at many of these junctures, rather amusing. In particular, starting to notice different reasoning put out on signs -- sometimes no biking/ATVing/etc is posted as a protection thing; but, here, for example, the reason given is "to reduce conflicts between users." That is not the real reason the PCT is only open to foot traffic & horse traffic. The real reason is that foot/horse travel only is part of the whole PCT wild-experience-thing. But, interesting framing here...

[![Trail scenery](https://farm3.staticflickr.com/2818/11136457945_ee5cc8dc9c_c.jpg)](https://www.flickr.com/photos/_ellie_/11136457945)

This is what the trail looked like today. I lagged behind Luke a lot, and felt pretty annoyed by my new shoes. After walking in them for a full day, they're just not the right shoe for me, I think. The arch is really stiff and the heel is way higher and also stiffer than what I was used to on the Cascadia's. So, while the size is better, and my blisters are much happier. The rest of my body is not digging it. I feel clumsy, and clunky, and just generally annoyed.

Also feeling troubled about my knee -- which still doesn't really quite *hurt* but doesn't feel right either, and is just mildly almost painful. So, really, this means I'm feeling uber paranoid (since it took me off the AT for a full week in 2008). This probably makes nothing better, since it just means I'm probably walking funny & making my ligaments do weird things. But, it's hard to ignore.

[![Green Valley](https://farm3.staticflickr.com/2869/11136453045_1b4dac62b6_c.jpg)](https://www.flickr.com/photos/_ellie_/11136453045)

My knee was especially bothering me on the descent down to Green Valley -- where Luke went on ahead and talked for a long time with a guy named Seeker, who somehow ended up out here because of a reddit post! Sounds like an interesting story, though we lost him after we hit the road, because his interpretation of the weird directions to the Anderson's on the signpost and our interpretations differed. We held fast to our interpretation -- especially once Luke double-checked it with his OSM app on his phone -- and this turned out to be a good choice!

[![Our location](https://farm4.staticflickr.com/3723/11136449115_d5de0acfbd_c.jpg)](https://www.flickr.com/photos/_ellie_/11136449115)

One of the Anderson's neighbors had this reminder of our place in the world on their front lawn.


[![Track Meat, Beads, Ole at the Andersons](https://farm8.staticflickr.com/7352/11136477156_23296c87a8_c.jpg)](https://www.flickr.com/photos/_ellie_/11136477156)

We made it to the Anderson's quite early -- around 2:30pm -- and after donning the requisite Hawaiian shirts, feasted on chips, salsa, beer from the gas station all afternoon. Made a run up to an amazing milkshake outlet up at the road crossing with Track Meat & Ole & Wild Child (she and Seminole came in just an hour or so after us) -- who fixed up a super good peanut-butter-banana-chocolate shake for me.

[![Afternoon chillin'](https://farm4.staticflickr.com/3670/11136470476_4fe0cc64a8_c.jpg)](https://www.flickr.com/photos/_ellie_/11136470476)

Turned out to be a lot less partying (or less rowdy-ness ) at the Andersons than I expected! And, Mr. Anderson seems upset to hear people were doing the whole 24 beers / 24 hours thing. I get the sense that they might be a bit overwhelmed with all the hikers here this year, but I'm not sure? Didn't really see them very much.

But there was LOTS of lovely relaxing. So enjoying all the people we're meeting out here. Trails are truly a special place for people. And, I was SO happy to have some WiFi on which to order new shoes from Zappos. Got some Cascadia's in a Men's 10.5 (!!) coming to meet me in Tehachapi + a pair of Women's Aasics in a size 12. Hopefully they arrive in time + one of them fits!

And, even being in civilization, after enjoying Terri's DELICOUS taco dinner, I enjoyed some lovely stars & a full moon this evening before crashing early in the most amazing manzanita forest in the back 'yard.' There must've been 50 or more campsites tucked away in various cleared out spaces in a huge grove of these desert trees. It was really a magical place.
