---
author: ellie
comments: true
date: 2013-04-29
layout: post
slug: day-9-a-random-boulder-field-to-scissors-crossing
title: 'Day 9: A Random Boulder Field to Scissors Crossing'
wordpress_id: 2065
tags:
- PCT
---

Had a LONG, hot hike down to scissors crossing yesterday. At some points, I thought it might NEVER end.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130429_103406.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130429_103406.jpg)

<!-- more -->

I like the way this hiker's yellow thermarest matches all the yellow flowers we were walking through :)

We had a nice break at the fire tank at Rodriguez Spur. Lots of hikers there picking up water and huddling under the bushes. Thankful for these fire tanks, don't know what we'd do for water without them! I wonder how many hikers they can sustain? What is their capacity? And how are they filled? I know some are filled from a fire truck that drives water up. Not so sure about this one, way up a dirt road.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130429_131351.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130429_131351.jpg)

At times like this, I'll admit I'm a little jealous of the people with the GoLite umbrellas!

Luke's watch read 92 degrees. Which, really, isn't THAT hot, but it still felt a little brutal without much shade after the tank at Rodriguez.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130429_172407.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130429_172407.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130429_174319.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130429_174319.jpg)

Even after we finally got down to the road-level flat desert.... the flatness just kept going and going and going. And then we got to the first road, and had to walk along a barbed wire fence separating it and the trail for what seemed, again, like forever... though I think it was probably something totally silly like a couple tenths of a mile.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130429_181008.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130429_181008.jpg)

(There was once water here)

Anyhow, made our way down to the bridge under the highway where there was not only a huge water cache set up by some lovely trail angel, but also a box of ICE!! It already had gotten dirt in it somehow or other, so we didn't have any frosty beverages, but my oh my, what a treat it was for our feet!!!!

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130429_184954.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130429_184954.jpg)

While under the bridge, we ran into [First Light](http://amaleeg.tumblr.com/), who wanted a hitching buddy to go to the Stagecoach RV park. We decided we could use a shower and laundry, too, so we headed back to the road to try to get a ride. More about this in our video recap for the week; but awesome magic from locals Tim & Marla who picked us up and then took us to their HOUSE! They fed us great food, provided some interesting company, and let us use their washer and shower!
