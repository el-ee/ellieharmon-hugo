---
author: ellie
date: 2019-12-19
layout: post
slug: making-18-per-hour
title: 'Making £18 per Hour Online'
tags:
- labor 
- technology
- work
- platforms
- research
---


![A screenshot of a task listed on the Prolific Academic website. The name of the task is "Emotional Situations 2." The reward for this task is £0.30. The Average Reward per hour is £18. The Average completion time is 1 minute, with a maximum allowed time of 4 minutes. ](../18hourtask.png)

As seen in the image above, retrieved from the Prolific Academic website in April 2017,[^1] workers on the platform are presented with statistical averages about available survey tasks. They can see how long the task has taken previous workers, and how the task-based pay translates into an hourly wage.

In some ways, Prolific Academic might be seen as a leader in the online labor space. Advocating for “ethical rewards,” they attempt to regulate payment on the platform, requiring that researchers pay out an average of at least £5 per hour to survey takers.[^2]

In the screenshot above, we can see that this particular task pays well above this minimum requirement. The average reward is a whopping £18 per hour.

Such a high hourly wage would seem to be a triumph for many in the worker advocacy community. It is 2.4 times the £7.50 national minimum wage in the UK (where the platform is based); and also soars above the catchy and ambitious “fight for $15” minimum wage that advocates are working towards in the United States,[^3] where I live and work.

Yet, the careful reader will also note another key statistic, the total payout per survey task: £0.30, one sixtieth of that glorious £18 per hour wage.

What does it mean to make £18 per hour, if one would have to string together 188 such surveys in a row, to make the ‘same’ wage in a given day as someone who makes only £7.50 per hour?[^4]

What if, instead of making £18 per hour, a worker in the UK made only the local minimum wage, £7.50 per hour, in one-minute increments? They would have to complete 2,250 tasks per week to make the ‘same’ amount of money as a “full time”[^5] minimum wage earner in that same week.

Is this rate of task-completion possible?[^6] If achieved, would the proper minimum wage (£7.50 / hour) provide for a good quality of life?

Beyond the challenge of locating and completing a full work-day worth of tasks on any given day, is it even possible for the worker to repeat this feat on a daily basis and ongoing for multiple weeks? What does this level of 'productivity' in front of a keyboard do to one's mental and physical health?

Where does the worker live, and how close are minimum wages to actual ‘living wages’? And, what if we took into account the extra costs and overheads of independent work over traditional employment -- the cost of equpiment, materials, paid (sick and vacation) leave, health and other insurance depending on country, retirement benefits, extra taxes, etc...?[^7]

At the end of the day, there's nothing ethical with this kind of payment structure for work, almost regardless of the dollar amount.


[^1]: Prolific Academic | Fair Crowd Work [http://faircrowd.work/platform/prolific/](http://faircrowd.work/platform/prolific/) Accessed 2017-06-26.

[^2]: This system is not perfect. Automatic enforcement only happens at the point of survey creation: when researchers enter the pay for a particular task along with an ‘estimate’ of how long the survey will take. For those who underestimate the time,  enforcement of the policy seems to be complaint-driven rather than automatic. See the section “Business Model” on the Fair Crowd Work profile of Prolific Academic: [http://faircrowd.work/platform/prolific/#platform-information](http://faircrowd.work/platform/prolific/#platform-information)

[^3]: At the time of writing, $15 is equivalent to £11.85

[^4]: Assuming a 7.5 hour workday:

    - 188 surveys x £0.30 per survey = £56.4
    - 7.5 hours x £7.5 per hour = £56.25

[^5]: Where ‘full time’ is understood to be 37.5 hours per week.

[^6]: And, for those who are paying for this work, how ‘good’ is the work quality if someone is rushing through 200-500 potentially quite disparate tasks per day? As Janine Berg has argued, current micro-task-based working arrangements are bad for clients and workers alike (Berg, Janine. 2015. [_Income security in the on-demand economy: findings and policy lessons from a survey of crowdworkers._](https://www.ilo.org/travail/whatwedo/publications/WCMS_479693/lang--en/index.htm) Conditions of work and employment series; No. 74. Geneva: International Labour Office, Inclusive Labour Markets, Labour Relations and Working Conditions Branch.)

[^7]: Although some online platform-based workers are traditional employees (see, e.g., Upwork Payroll [https://www.upwork.com/legal/upwork-payroll-agreement/](https://www.upwork.com/legal/upwork-payroll-agreement/)), these workers are already subject to existing labor laws including minimum wages. So they are necessarily excluded from the question of this project — can and should minimum wages be applied to platform-based work? For this essay, I only focus on the situation of “self-employed” or “independent” crowd workers. Indeed, I think the re-designation of crowd workers as actual employees is a good course of action for future advocacy work, rather than piecemeal working through each individual labor benefit and re-applying it one at a time to platform-based work. See also (Berg 2016).

_NB: I've been holding on to a draft of this one for a while! If you enjoyed this 2-year-aged post, find more like this in an ILO report I contributed to:_ Janine Berg, Marianne Furrer, Ellie Harmon, Uma Rani and M. Six Silberman. (2018) _Digital labour platforms and the future of work_. International Labour Office, Geneva. <https://www.ilo.org/global/publications/books/WCMS_645337/lang--en/index.htm>
