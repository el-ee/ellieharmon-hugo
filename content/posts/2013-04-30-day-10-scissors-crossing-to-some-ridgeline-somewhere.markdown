---
author: ellie
comments: true
date: 2013-04-30
layout: post
slug: day-10-scissors-crossing-to-some-ridgeline-somewhere
title: 'Day 10: Scissors Crossing to Some Ridgeline Somewhere..'
wordpress_id: 2082
tags:
- PCT
---

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_085222.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_085222.jpg)

Marla and Tim took us up to Julian this morning for our free piece of PCT hiker pie at Mom's Pies... after making us scrambled eggs, bacon, toast (with local jam!). And then drove us back down to Scissors Crossing where we got back to hiking! SO MUCH THANKS to them for being SO awesome. It's the unexpected community of the trail that makes these journeys such a great experience. Coming away from my AT hike in 2008, the biggest thing that stood out to me about the hike, was having my faith in humanity and strangers restored. This trail, perhaps even more so.

<!-- more -->

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_105515.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_105515.jpg)

We had a long, but pleasant hike up from scissors. Nicely graded trail -- again thankful that this is a horse path, too, and not just a people hiking path :) And thankful for all the work that's gone into making it!

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_111537.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_111537.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_111637.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_111637.jpg)

We got some new desert scenery -- ocatillos and cholla cacti!! More like the Joshua Tree landscape we are familiar with.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_122928.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_122928.jpg)

And, we also walked through a large burn area, where I couldn't resist taking a THIRD sample of soil to send back to Christina, even though we are still in "Section A" of the trail.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_181602.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_181602.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_182351.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130430_182351.jpg)

A few hours into our hike we made it to the infamous "third gate cache" -- which was amazing. Like the fire tanks that have been providing our water source so far, it would be so much harder to hike this trail if it weren't for caches like this one.

I was kinda bummed to find it a bit of a mess though. At least 50 or so bottles not tied up, and, so, lest they get lost in the desert wind, I spent quite a while tidying up around the cache (much to Luke's chagrin! He was ready to get some more miles in!) There's a video of me organizing that might make an appearance at some later date...

But, we managed to get a few more in just before dark, anyway. Camped on a VERY windy ridge with a few other folks -- Sphinx, who we've seen several times, and a couple from SF who live just a few blocks from Luke! What a small world.

I was really mesmerized by all of the stars we could see from up on the ridge. We were far enough away from light pollution of cities, and had really clear skies -- I even saw a shooting star, right overhead! And couldn't help but think about how much other life there must be out there. Seeing planes fly back and forth and getting buzzed by helicopters (looking for immigrants all I can figure), a little strange to think that this US world of moving people and things around, dominating space with our technologies is how we might choose to spend this life, rarely satisfied with being here or there, but so frequently on the move and so concerned about regulating who is (allowed to be) moving where.
