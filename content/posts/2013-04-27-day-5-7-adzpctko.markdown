---
author: ellie
comments: true
date: 2013-04-27
layout: post
slug: day-5-7-adzpctko
title: 'Day 5-7: ADZPCTKO'
wordpress_id: 2052
tags:
- PCT
---

We took our first three zeros at the [ADZPCTKO](http://pct77.org/adz/) (annual day zero pacific crest trail kickoff) held back at the Lake Morena campground.

<!-- more -->

First, Thanks to [Dicentra](http://www.onepanwonders.com/) for giving me a ride from Mt. Laguna, and to Doug for picking up Luke 'on his way' down from Davis. Doug is hoping to hike the trail next year, and like we did last year, came to kick off to learn a few things from current hikers and start preparing. He also gave us a ride back to Mt. Laguna on Sunday along with four other hikers heading a bit farther north! We look forward to following his journey next year :)

Kick off is a great event for meeting other hikers, learning some wisdom about the trail, etc. Especially enjoyed Condor's talk on journaling. If you haven't seen his website, you might enjoy checking it out: [thehikeguy.com/](http://www.thehikeguy.com/). Despite the lack of updates on here, I've been doing a good job making time for my own personal journaling so far. And, this was good inspiration to keep at it. Also, some good reasons to try to do both. As Condor put it, "there's sharing and then there's journaling..." and while "I'm not a troglodyte... I instagram the hell out of trails..." a written record of thoughts and experiences _for yourself_ serves a different purpose.

Some pictures from around the weekend:

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130426_132802.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130426_132802.jpg)

Hundreds of thru hikers == hundreds of dirty socks. A lot of people had already started the trail, like us, and had hitched back to kickoff. So, lots of showering and laundry trying to get done in between hanging out, drinking beers, eating burgers, buying new gear, and listening to talks.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_071444.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_071444.jpg)

Luke charging phones outside the pavillion, enjoying some morning sunshine.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_071916.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_071916.jpg)

A spot where I heard Verizon customers can get reception. There was often someone sitting here on their phone. So far AT&T customers are winning the cellular infrastructure game.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_103623.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_103623.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_105756.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_105756.jpg)

Luke & I volunteered for the silent auction to help raise money for next year's ADZPCTKO.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_145851.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_145851.jpg)

A crowd watching the homemade gear show.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_151013.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_151013.jpg)

Our campsite buddy, [Aaron](http://aaronjnicholson.com/) -- near the left side of the lineup of competitors -- won the contest with his custom tyvek/mesh bivvy!!

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_155706.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_155706.jpg)

Thru-hikers, past and present, head out to the rocks by the lakeshore for class pictures.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_192715.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_192715.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_194110.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_194110.jpg)

Night-time movie showings on a custom Tyvek (of course) screen. First night: shorts by the community, mostly from last year's thru-hikers. Second night: [the 2012 class film](http://vimeo.com/65233793). Gave us lots of inspiration and things to look forward to!

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_192910.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130427_192910.jpg)

And in closing, Luke documents his continuing beard growth :)
