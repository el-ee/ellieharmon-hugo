---
author: ellie
comments: true
date: 2013-04-16 03:26:11+00:00
layout: post
slug: packing-and-preparation
title: Packing and Preparation
wordpress_id: 1946
tags:
- PCT
---

I feel simultaneously  under- and over- prepared for this trip right now. People keep asking me about what I'm doing, etc. And I never feel like I have a good answer.  Ethnographically, I feel like haven't done that much. Lilly keeps harping on me to write some notes down, and Katie and Marisa, too, so trying to make some time this morning for that... (started this on Sunday — only a day behind getting it finished. Not bad!)

<!-- more -->

Most of my preparation feels like it has been geared towards preparing to be away from other stuff — cleaning the apartment in SF to get it ready for Lynn; cleaning the apartment in Irvine to get it ready for Lilly. Packing up clothes and making space in the closets; trips to goodwill; trips to the storage unit in SF.

And trying to prepare Melissa and Christine for my being gone — working on typing up lots of field notes that were I working alone, would just stay in my handwritten notebook forever instead. And were I not leaving, might stay in my notebook, too... feel like my planned inaccessibility creates an impetus for doing more work on them and sooner.

Of course, that's not to say that there has been no preparation for the hiking. Luke and I managed to squeeze in two short backpacking trips to test out [the new gear](http://ellieharmon.com/2013/04/we-have-a-lot-of-new-gear/) in the last couple of weeks. We went out to JTree around here and then went out to Point Reyes up in SF week before last.

More on those trips and Luke's sleeping bag hacks later...

[![photo of preparation](http://farm9.staticflickr.com/8540/8654091800_d6cbc3cd93.jpg)](http://www.flickr.com/photos/_ellie_/8654091800/)

I have also been packing up resupply boxes. At this point, I think we are pretty well covered from the Mexico border up to one resupply past Ashland, OR. I am thinking that when we get to Ashland, we can reassess the situation and pack our own boxes for the rest of OR and WA. I am hoping to be treated to visits somewhere in Oregon from [family](http://waywardspark.com/) and [friends](http://dillonm.com/) who live in the state. So, I think it makes sense to wait on making any of that too concrete until we get a bit closer.

In any case, I am not really sure if even the Southern California boxes would be packed right now if Katie hadn't been on my case about wanting to come over and see what it is I am expecting her to do in mailing them off to us over the next few months. So, that was good pressure last week to get something practical done. She stopped by my desk at work on Monday or Tuesday to see what was up and suggested we meet Thursday evening. So, Thursday late morning, I thought, oh maybe I should go take care of some of this shopping! It took ALL day. I didn't get home from it all til after 4pm. I had no idea it would be so involved!!

First, I spent a couple hours trying to figure out a plan so I knew what I needed to buy. Craig's PCT Planner has been invaluable. Made a small paypal donation a few weeks ago while I was using the site & got an email back from him saying thanks and that he was thinking about hiking himself this year. When I logged back into the site this week, I saw a note up in the corner saying that it was indeed happening (and asking for any last minute requests people had before he because inaccessible. Sounds like his preparations must be somewhat like mine — warning and preparing others' for his going away!) I'm so excited he's actually going to be out there hiking himself this year! I think it would be great to meet him.

In any case, the site has been lovely for checking boxes and reading out the times between resupply stops for getting the packages planned without having to do all the math myself. Though it makes me feel a bit under-planned as well. Knowing we'll never hike the speed that I put into the little boxes there. Knowing we'll get off schedule. And the specificity of it (Saturdays and Sundays show up in red!) makes me worried about getting into a hiking pattern that involves stops at the post office on days when they're closed. But I'm trying hard not to over think it as much as the neat spreadsheet of numbers I can export encourages me to get lost in calculations and planning.

Here is our overly-ambitious plan.

[![Screen Shot 2013-04-15 at 4.57.39 PM](http://ellieharmon.com/wp-content/uploads/2013/04/Screen-Shot-2013-04-15-at-4.57.39-PM-911x1024.png)](http://ellieharmon.com/wp-content/uploads/2013/04/Screen-Shot-2013-04-15-at-4.57.39-PM.png)

My mom will be joining us for the segment in Northern California with the 10-mile days. Would VERY MUCH LOVE to see some others of you while we're out hiking this summer. So, do stay in touch if you're interested :) But also do know that this schedule is a totally crazy approximation, and we will undoubtedly be both ahead of it and behind it all the time. Hopefully never so far off that we starve on the segments that I've already planned food for ;)

Anyhow, this whole thing also makes me feel over-planned. I mean, I took off on the AT five years ago with no preparation at all. I decided to go after a brief Facebook conversation with [this guy](http://leftorium.net), gave 2 weeks notice at my job, somehow rented my apartment, sold everything that wouldn't fit in my little Honda Accord, and (with the amazing help of my Mom) packed and drove the rest to my parents' house in the exurbs of Atlanta (where much of it still sits in boxes...) and was on the trail something like 16 days later. I bought most of my food along the way, and had AMAZING help from mom as the resupply queen at home. I could call her from a stop and ask for 5 dinners, and 4 lunches and snacks and she'd throw a box together and put it in the mail the next day so that when I arrived at my next stop, there it was waiting.

I remember laughing at people who had crazy Excel spreadsheets of their resupply plan. This year, I am trying to do it all in advance, and it's terrifying. TERRIFYING.

In any case, on Thursday after all my scheduling calculations/freak-out, I went shopping at no less than FOUR stores. First stop, REI. I was in desperate need of new trekking poles as my last ones are literally splitting at the seams. They are Leki's and I think come with some lifetime warranty, but I already cashed in on it once midway through my AT hike (guy at REI when I go to return them asks, "what did you do? fall on them?" me: "yes, like 100 times?!") and felt like I was due for actually purchasing a new set. While there, also grabbed some of the latest and greatest DEET alternative ((P)icaridin, which, like most things, has been available in Europe already for AGES now); some freeze dried dinners (and desserts!); some hopefully-not-itch-inducing sunblock (this happened on the JMT last year. Thx Neutrogena.); and a bunch more various bar like things that will probably make me want to vomit in a few weeks.

So, then, the groceries (oatmeal, tuna packets, mac and cheese, starburst and jolly ranchers, crackers…). I started at Sprouts because it's right there near the REI in Tustin. But, no. Sprouts, despite being not-fancy-enough for my usual grocery shopping is actually TOO fancy for individually packaged oatmeal. So I knocked off about half my list there, and then headed over to Target. Fulfilled my oatmeal desires there, but no luck on mac and cheese of the microwave variety (handy for hiking because it requires only water and not milk)… so I ended up making a THIRD stop at Whole Foods (in retrospect, maybe should've just gone there first. Commercial enough for the oatmeal packets while also having potentially not-vomit-inducing instant coffee, also microwave-style mac and cheese of the organic variety. Haha.).

![](http://instagram.com/p/X-_-fMOYpe/)

Finally picked up boxes on my way home, texting Katie at 4:30, hoping that she wasn't already at my apartment waiting on me. She wasn't (whew!) so I headed home to begin the packing. I had only just gotten things out of the bags when she arrived, but she assured me that I looked TOTALLY organized.

And, so we headed out to dinner at Veggie Grill where I got to enjoy the company of her 2 always-adorable kiddos and her delightful brother who was hanging out in SoCal after spending the winter in Oakland, and wanting to delay his return to still-somewhat-wintry Washington another week or so.

[![Kaite & her kiddos](http://farm9.staticflickr.com/8102/8652990597_4b7d8ba90c.jpg)](http://www.flickr.com/photos/_ellie_/8652990597/) [![Calculating by iPhone @ Veggie Grill](http://farm9.staticflickr.com/8401/8654091342_7d9872b9d5.jpg)](http://www.flickr.com/photos/_ellie_/8654091342/)

Post-dinner, I did manage to get most of the boxes all packed up and before Lilly came home from all night dissertation writing on Friday evening, I had actually managed to clean up the living room enough that we both could sit down and watch the Mad Men premiere (I didn't make it through the whole ~2 hour episode myself... I feel like I am really missing something with that show. I keep trying. But it just doesn't do it for me.)

[![prep photo](http://farm9.staticflickr.com/8101/8654218832_95abc3eec9.jpg)](http://www.flickr.com/photos/_ellie_/8654218832/)

Other stuff: making time for amazing friends who I don't see often enough, and in the course of it, actually spending my first day IN LA since moving to SoCal. Visited the Museum of Jurrasic Technology AND roller-skated from Venice Beach to Santa Monica -- recommended!!

[![A Flier at the Museum of Jurassic Technology for theiff.org](http://farm9.staticflickr.com/8536/8654091144_d4af6ed9f4.jpg)](http://www.flickr.com/photos/_ellie_/8654091144/)
[![LA Sunset](http://farm9.staticflickr.com/8255/8654369058_6cf5ff34b9.jpg)](http://www.flickr.com/photos/_ellie_/8654369058/)

All the while... have been writing a grant with the super-awesome [Christina Agapakis](http://agapakis.com/) about an art and science biogeography project we are planning about the soil of California while I'm out on the trail... but I will have to save the details on that for another time… because this is already long, and that project deserves more attention than the closing paragraph of this post about packing. I am SUPER excited about it (and several 'citizen science' projects I keep getting myself involved in). So check back later in the week. I will try to get something up before I disappear for the first week of hiking! (SO SOON!!!)
