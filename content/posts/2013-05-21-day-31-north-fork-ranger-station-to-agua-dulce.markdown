---
author: ellie
comments: true
date: 2013-05-21
layout: post
slug: day-31-north-fork-ranger-station-to-agua-dulce
title: 'Day 31: north fork ranger station to agua dulce'
wordpress_id: 2351
tags:
- PCT
---

[caption align="alignnone" width="500" id="" caption="Serious trail work! There is a LOT of labor and sweat that goes into making this hiking thing possible." ][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_072851.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_072851.jpg)[/caption]

We briefly chatted with the ranger at North Fork this morning while he was out walking his dog before beginning our trek towards Agua Dulce, and the much talked about Saufley's. We thanked him for the awesome water cache and he gave us an update on what lies ahead on our trek today.

[caption align="alignnone" width="500" id="" caption=" A sampling of our morning hike. I swear we hi the top of EVERY single little hill out here. For seemingly no good reason. Walking the 'crest' I guess... " ]
[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_080835.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_080835.jpg)[/caption]





<!-- more -->





[caption align="alignnone" width="500" id="" caption=" Rad trail magic!!! " ][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_100536.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_100536.jpg)[/caption]

Not too long into our walk, we came down to Soledad Canyon Road. We could see this road for a LONG way before we got to it, and, several times, we crossed over a nicely graded dirt road that we could tell ended in the same trailhead parking lot we were going to. The road skipped several little hills, and went comparatively straight for the parking lot.. I was pretty tempted to take the road, especially after we ran into a rattle snake on the trail near one of the many crossings... but Luke talked me into toughing it out on the winding and hilly trail. Something about us not being out here to hike on a road or whatever... and IT WAS SO WORTH IT! Right there around the last bend of the trail as it came into the parking lot was a little cache of chips and gatorade! We were so hot and thristy and so happy to arrive at this little treat! We heard later that our old friend Aloha was responsible. Cheers to him!

[caption align="alignnone" width="500"  id="" caption=" An information sign at the Soledad Canyon Rd trailhead... these days, it'd be pretty unusual, and pretty frowned upon, to be still in California in September. Most people are aiming to finish by early to mid September in Canada! I've heard that 'back in the day' hiking schedules were quite different.. a longer hike, with more snow encountered on both ends..though it's unclear to me if even on a longer schedule/timeline, one would want to be in 'central CA' still in September?? The lightweight gear that we have today certainly enables a whole different kind of hiking style these days that flies through the 'easy' parts and attempts to avoid most of the snow entirely. " ][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_104352.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_104352.jpg)[/caption]

After a brief gatorade and chips break in the half-shade of a picnic table at the trailhead parking, we headed across the highway -- and being recently refueled, we decided to skip the detour that many hikers take to a nearby KOA (which has in addition to camping, a pool and a small store). We were ready to get to Agua Dulce!

[caption align="alignnone" width="500" id="" caption=" The official monument memorializing the completion of the PCT... in the middle of nowhere, beside a road and some railroad tracks. Lovely.. " ][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_105948.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_105948.jpg)[/caption]

Not long after crossing the road, and a set of railroad tracks, we came to this monument designating the official completion point of the PCT. We read later in a book that the routing of this section was quite silly because it was the last thing to get done, and the PCTA et al just wanted to finish so badly they gave into some crappy deals with all the many land owners in this part of the state.

[caption align="alignnone" width="500" id="" caption=" The highway 14 culvert -- really the whole light at the end of the tunnel expression fit pretty well. Once we got across this, we had only a few more short miles to the promised land of Agua Dulce and the appropriately named 'Hiker Heaven' hosted by Donna Saufley.!! " ][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_132144.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_132144.jpg)[/caption]

After Soledad Canyon Rd, the next big landmark of the day was highway 14. It again felt like we could see this road FOREVER before we crossed it. The trail parallelled the highway for at least a couple miles, but we finally reached the famous culvert, and took some nice pictures of our own. (The shape of the tunnel here is essentially the same shape as the PCT logo which adorns trail markers, trail angel houses, the cars of good people, etc.)





[caption align="alignnone" width="500" id="" caption=" Luke in the tunnel "] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_061725.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_061725.jpg)[/caption]




Nearing Agua Dulce, we had a lovely, if brief, walk through Vasquez Rocks picnic area. This is definitely a place I would come back to and explore sometime, but by the time we reached it in our hike today, we were tired and hot and so very thirsty. The water at the picnic area (which only came out of fountains designed for animals) tasted less than great. I tried t improve it with some gatorade powder we'd purchased in Wrightwood, but it was some kind of funky new flavor that turned out to be EVEN WORSE! I don't know what's up with all this 'arctic rush' shit. I mean, that's not even a flavor!? Experience marketing/design. Yuck. In any case, we kept on trucking through this park, making a beeline for the grocery store, with cold and better tasting beverages (and ice cream). This is definitely a place I'd love to come back to sometime (with plenty of water).




[caption align="alignnone" width="500" id="" caption=" I was really excited when I first saw these signs - I thought they were names for the rocks! I was like, oh cool! A  rock named wild cucumber! That's awesome! .. but no.. there was actually a wild cucumber plant under the sign. How boring. " ][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_135519.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_135519.jpg)[/caption]





[caption align="alignnone" width="500" id="" caption=" Old School Americans. Our heroes today! " ]
[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_063211.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130522_063211.jpg)[/caption]

When we arrived at the Saufley's, they were sadly already over capacity! And it was hardly 3pm! Clearly there are a lot of us out here this year... and we are right in the big herd.

But, being the lovely lady that she is, Donna took quite good care of us even if she was over capacity. She called up a neighbor who has a small goat farm nearby (and is getting into the dates, pistachios, etc. business in the next few years), and these lovely folks took us in for the night. I think, in the end, we got a pretty good deal -- we ended up having the farm office all to ourselves, where we set up on a great super long sofa, enjoyed some very fresh and cold goat's milk, took showers without having to wait in a long line of other dirty hikers, and generally had some time to just chill out without being in the middle of 50 other hikers.

[caption align="alignnone" width="500" id="" caption=" Some DELICIOUS fresh goat's milk! " ] [![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_154015.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_154015.jpg) [/caption]






[caption align="alignnone" width="500" id="" caption=" These people clearly know what they're doing when it comes to raising animals! " ][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_170557.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130521_170557.jpg)
[/cption]

I spent some time talking to my mom on the phone, and attempting to write up a few blog posts. We walked back into town for mexican food at the local cafe -- great micheladas, horrible queso dip. Can't have it all I guess.

And then I worked at cutting away some of the dead skin and dirt stuck all up under it on my blisters under my toes. I am so ready for some new shoes, these things are getting SO gross. I seriously nearly passed out doing it. There are like five layers of blister on my right foot :( But at least the one under my left big toe seems to be healing. I think there is still a little one deep underneath, but for the most part it's ok. I am getting another new one though on the back of my left heel... and that one is just getting bigger by the day. QQ. etc... Hoping that some new bigger/wider shoes will do the trick and get these things for real healed up. We're also planning to take a zero tomorrow (hopefully at the Saufley's -- and get in some hiker friends time) so the rest should be good for the poor feet.
