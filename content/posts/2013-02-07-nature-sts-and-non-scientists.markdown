---
author: ellie
comments: true
date: 2013-02-07 03:38:47+00:00
layout: post
slug: nature-sts-and-non-scientists
title: Nature, STS, and non-Scientists
wordpress_id: 1590
tags:
- ethic
- lorraine daston
- mundane
- nature
- objectivity
- ordinary
- peter galison
- science
- sts
- technology
---

Really enjoyed reading Daston & Gallison's Objectivity this week at the behest of Beth. In my reading and note-taking, some questions and thoughts that came up for me about objectivity, science, scientists, technology, and a question about where and how do more ordinary people -- or at least more mundane practices -- fit in to such research? 


<!-- more -->


This book, is, in the author's words, "...about how and why objectivity emerged as a new way of studying nature, and of being a scientist" [p. 17]. I like the work in this book to destabilize arguments of straightforward cause and effect, and to begin mapping changes in epistemologies, stakes of knowledge and its making, ideas of nature in broader "avalanches" [p. 50] of culture and selfhood and virtue. The authors direct us in the first chapter towards a question of "What deeper historical forces -- intellectual, social, political, economic, technological -- created this novum?" [p. 35] Their answer, "the emergence of objectivity must tally with the emergence of a certain kind of willful self … the history of objectivity becomes, ipso facto, part of the history of the self. Or, more precisely, of the scientific self" [p. 37]





That is, they condense these "deeper historical forces" into little more than a self. On the one hand, selves seem like good places of condensation and sedimentation. And the attention to scientific self-hood makes this a really interesting and lovely book, distinct from the (perhaps limited) set of science studies/science history books that I've read previously. It's highly successful in shifting our attention a bit by pointing away from a regularized mapping of cause and effect from the invention of the camera/statistics to the development of objectivity.





Kavita commented that others have critiqued the book for its exclusion of colonialism in it's history. Certainly this seems a critical point for a book that spends so much time dealing with the scientist figured as an explorer and adventurer, working in the politics of a time that involved setting off on ships and expeditions to bring back drawings and specimens from distant lands. More generally, the self here feels overburdened, being asked to stand in for such a multiplicity of forces, and simultaneously, a little empty. They mean self more figuratively than personally or experientially -- "The Scientific Self" not "this particular scientist's lived self" -- but nonetheless implicating the personal, leaves me wanting for more attention to the practical economies of atlas publishing, the shifts in theology and religion, the gendered relations of scientists/authors/artists/drawers/taxidermists/machine operators. All of these things are mentioned in brief sentences or paragraphs, but, even in the bounds of a 400+ page book, don't find much space.





Along these lines, I appreciate the arguments about epistemic and moral virtues as being simultaneously "distinct as ideals" and "loosely coherent" but far from "strictly internally consistent." I often feel like disciplines can seem to swing back and forth, as if on a pendulum, between focusing on <x> at some time tx, and then on <y> at some time ty, and then back to <x> at tx' etc... Take STS: Arguments always positioned in contrast to whatever else is currently popular seem to tack back and forth, navigating a sea of change through calls for more agency of objects and then more social construction of knowledge/technology and then more materiality and then more culture and affect. These swings aren't neatly chronological. Someone writing today, might as easily be writing in response to a book or article from 1970 as 1990 as 2010 or any point in between. But, any one thread of citations and argument seems often traceable in a zigzagging way backwards through arguments for a focus on technology/objects/materiality and a counter-arguments for a focus on social/cultural/selves. The sedimentation and accumulation metaphor helps us keep in mind the ways that science today might be asked to be accountable to science of yesterday, and 50 years ago, and 100 years ago, and 500 years ago -- despite differences in definitions of what counts as 'true' or 'knowledge' or 'science.' And, so, like any good suggestion, I want more of it. The layers depicted in the openly simplistic chart in the last chapter make the old questions of objectivity appear buried and more obsolete than their argument would seem to suggest. I want sedimentation with plate tectonics and earthquakes and volcanoes... and some kind of less violent and punctuated seepage.





There's lots more of interest here, for me, in particular about the ways that technology and nature are figured across the three broad periods that D&G stake out. Although the self and objectivity are the aspects of these configurations that the book highlights, we pick up on the side numerous other shifts. Implicitly, science seems continually to be about seeing/knowing nature with the aid/incorporation of various devices. In the period preceding what D&G term 'truth-to-nature' we read that science aimed to document the obscurities and deformaties of a wild and monstrous nature. Then, alongside shifts in Christian theology wherein God's beauty comes to be seen in grand and perfect plans despite the flaws in any one individual, the goal of science shifts towards seeing through individual imperfections, to know nature through transcending the individual and finding the taxonomy of destined ideal types. As mechanical objectivity comes about, there is a shift back towards individual artifacts, and documentation via camera obscura/photography of particular specimens... though not the same shift back to the concern with documenting variance. Here the desire for more directly produced images comes from an anxiety about human variance, and the untrustworthiness of individual perception. The goal is to see and observe without interference. Technology and the scientist shift places. Previously the scientist was tasked with touching up images, correcting technological outputs, and working with artists to depict an imagined 'ideal'/generalized specimen from numerous singular examples. In this new phase, it is instead technology that is meant to keep the scientist in check. I'd be interested in thinking through these configurations of science/nature/technology from different/more angles than objectivity/subjectivity.





And in so doing, how to do it with more ordinary moments of living, than reading/writing science atlases/scientific journals. What would this book look like if we took the same questions and looked at theological writings, or focused on politicians or elementary school teachers? What does it look like with workers, families, people who spent excessive amounts of time outside? (See there, those are my fieldwork participants. Neat, huh.) Seriously, though, this is one thing I LOVE LOVE LOVE about Emily Martin's Flexible Bodies. We get this amazing mix of perspectives on immunity and ideals of flexibility traced through immunology research labs, an AIDS service organization, ACT UP, 'regular' people from some supposedly broad sampling of Baltimore neighborhoods, and even workers on a corporate ropes course retreat.





All of these people still have stakes in contest over what's natural, what's scientific, what's factual, what kind of knowledge is valid and action worthy. Who is asking the STS questions with not-scientist-participants? How can we bring STS perspectives on nature, technology and knowledge-making to fieldwork tuned towards the mundane practices of daily working, living, and being? If science and society (selfhood, all of these "deeper historical forces" of D&G) are all so intertwined, why scope such a vast majority of projects around 'scientists' as a category? As Martin writes, "the very borders of the sociological entities such as work, family, community, and science are often in flux, it did not seem advisable to being with them as the units of analysis" [p. xvi] Who else is doing work like this?
