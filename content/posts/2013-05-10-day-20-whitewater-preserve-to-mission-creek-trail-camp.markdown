---
author: ellie
comments: true
date: 2013-05-10
layout: post
slug: day-20-whitewater-preserve-to-mission-creek-trail-camp
title: 'Day 20: Whitewater Preserve to Mission Creek Trail Camp'
wordpress_id: 2172
tags:
- PCT
---

[caption id="" align="alignnone" width="500"][![Walking through a burn](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130510_141629.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130510_141629.jpg) Walking through a burn[/caption]

Long hike today first along the creek that runs through whitewater preserve, and then over a small ridge and up mission creek. Much of the area had burned in a somewhat recent fire. So, it was quite exposed and HOT! However, we did get to enjoy our first creek long enough to lay down in (though not deep enough for a swim. It felt AMAZING at our lunchtime break.

<!-- more -->

[caption id="" align="alignnone" width="500"][![Cool tree weathering](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130510_185709.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130510_185709.jpg) Cool tree weathering[/caption]

[caption id="" align="alignnone" width="500"][![Our First Poodle Dog Encounter](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130510_172238.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130510_172238.jpg) Our First Poodle Dog Encounter[/caption]

One of the plants that we've heard SO MUCH about is the 'poodle dog bush' -- it is a fire follower, so we know we'll be seeing it in several upcoming burn areas. Apparently it is a relative of poison oak, and has the same chemical in it that causes itching and blistering, only it is supposed to be much much worse. We heard lots of stories at kickoff about people ending up in the hospital because their reaction to it was so bad. We were careful to avoid it, which was pretty easy in this area. There were only a few small plants like this one, and they weren't yet blooming (which apparently is when they are they most poisonous).

[caption id="" align="alignnone" width="500"][![Just a sign from today, one of our many boundary crossings](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130510_165942.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130510_165942.jpg) Just a sign from today, one of our many boundary crossings[/caption]

I'm really enjoying the dirt project I'm doing with [Christina](http://agapakis.com). If it weren't for how heavy it would be to carry it, I'd probably collect 10 samples a day! Haha. Have been really paying attention to the ground I'm walking on this year, and amazed at how fast the dirt and the rocks change beneath my feet, and the bigger landscape of plants and animals. Although we're mostly in 'desert' out here so far, it varies quickly with elevation, at the edge of a burn area, across a ridgeline, at springs, at creek edges and just a mere 100 yards away. Rocks are green, orange, purple, black, grey. The mountains are often striped. I wonder if the life in the soil changes as fast as the appearance of the colors, minerals, rocks that comprise it?

[caption id="" align="alignnone" width="500"][![Saw lots of new growth coming out of burned-looking trunks. Also didn't know yuccas like this grew with trunks!](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130510_153817.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130510_153817.jpg) Saw lots of new growth coming out of burned-looking trunks. Also didn't know yuccas like this grew with trunks![/caption]

The hike to Mission Creek Trail Camp wasn't terribly long, but we did a TON of elevation -- over 7000 feet today I think. So, by the time we got there, we were exhausted -- more tired than I've been on any day yet! I think that we might not have made it if it weren't for how refreshed we were last night after our lovely visit to Ziggy and the Bear's. There is a kind of euphoria that stays with you after encountering such nice strangers -- I can see why Cool Ranch and Capitan want to spend their summer volunteering at angel houses this year.
