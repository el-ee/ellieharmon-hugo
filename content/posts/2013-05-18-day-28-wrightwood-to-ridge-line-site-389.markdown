---
author: ellie
comments: true
date: 2013-05-18
layout: post
slug: day-28-wrightwood-to-ridge-line-site-389
title: 'Day 28: Wrightwood to ridge line site ~389'
wordpress_id: 2303
tags:
- PCT
---

Woke up well rested this morning but before the local breakfast places were open. So, we headed over to a nearby coffee shop where I did some blog posting and Luke got to call and talk to his Dad for a while.

Much to our surprise, upon ordering our coffees, the barista asked,  "are you PCT hikers?" -- "Yes" (our permanently dirty clothes give us away I guess, even after showering and everything! Maybe also the lime green down jacket I have. Not quite LA tourist style I suppose.) -- "Then coffee's free. Can I take your picture for our book?"

I'm still a bit surprised how nice and generous lots of local businesses are to hikers. It's not clear to me what's really in it for them. But, if anyone has cause to visit Wrightwood, look for us in the photo book of hikers out on the tables at the coffee shop.

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130518_075805.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130518_075805.jpg)



After coffee, we continued our tour of SoCal biscuits and gravy side dishes. This place did pretty good ones as I recall. I'm still a bit surprised its an option instead of toast with most meals and not an entree of its own. But, whatever. Works for me!

We got another super nice ride back to the trail from a woman who works at the local high school and was on her way to her son's baseball game in somewhere near Orange County like San Clemente maybe? Its funny to run into all these people headed that way and strange to be only an hour from LA. Still.

As usual, she didn't want any gas money. As she put it, "its only five seconds up the road!" It's really more like 10 minutes, but she spent half the car ride talking about how they love to drive around here, and that "everything" is an hour away. Shopping, work, baseball games, etc. I'm glad the local supermarket continues to survive despite that everyone capable of doing so seems to drive an hour to shop at cheaper, bigger places. It makes a big difference to people without cars to be able to shop locally!

[caption id="" align="alignnone" width="500" caption="A sign reminds us of just how little we've hiked so far"][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130518_102441.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130518_102441.jpg)[/caption]



[![image](http://ellieharmon.com/wp-content/uploads/wpid-PANO_20130518_132825.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-PANO_20130518_132825.jpg)



[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130518_134351.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130518_134351.jpg)



Long walk today, and I had a rough time with my blisters bothering me, but we enjoyed the views from atop BadenPowell (along with millions of boy scouts).

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130518_135005.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130518_135005.jpg)



[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130518_135343.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130518_135343.jpg)



Our lunch was pretty exquisite, eating away at the weight of all the fancy treats that we scored in town :)

Was catching up on old blog posts this morning and writing about the water report some days ago. And this has been coloring my thoughts today. Thinking more about information infrastructures and (in)visibility of information on the trail. Like in wrightwood, staying at trail angel houses seems pretty popular and all the information you need to do so is 'public' -- it's just written down in a notebook lying out on the counter at the hardware store. But the info itself is not published in yogi's book or directly in Halfmile's maps. They just tell you where to look and you go look in person. Which means it's kept up to date and can be changed easily at the whim of anyone providing services. Just show up to the store and change what you wrote. And it's physical obscurity seemingly provides some protection or security to the people who are listing their addresses and phone numbers? Anyhow, just paying attention to the ways various kinds of information move around out here.
