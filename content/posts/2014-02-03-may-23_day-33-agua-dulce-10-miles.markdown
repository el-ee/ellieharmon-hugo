---
author: ellie
comments: true
date: 2014-02-03
layout: post
title: '2013-05-23 / Day 33: Agua Dulce + 10 miles'
tags:
- PCT
---

[![Bicycle fixed up by Puppy, yesterday.](http://farm3.staticflickr.com/2836/11136659404_20edb2ae71_b.jpg)](http://www.flickr.com/photos/_ellie_/11136659404/)

Slow morning today. We biked into town upon waking up, which was pretty much the best feeling ever -- moving so fast with the wind in my hair! Though I was a little impressed by just how weird it was to move different muscles. Climbing the little hill on the road wasn't quite as easy as I thought it would be!

In any case, when we got to town, we found Dragonfly and Beaker at the same cafe we visited yesterday. They had stayed yesterday at the goat farm where we were night before last, and seemed to be looking forward to taking a zero for themselves today at the Saufley's. We enjoyed breakfast with them, where the topic of ice axes came up again. We told them we had decided against getting any, but they are still undecided.

Ned of the winter mountaineering school has been posting lots of stuff on the Facebook group about how people need to bring them, but we are pretty unconvinced, and don't really trust his word. He seems to hype snow a bit too much. Beaker seemed to agree, saying that he stopped reading his PCT-L posts a long time ago. It's like the boy who cried wolf or something. Even if there was real snow out there, I wouldn't believe it if Ned was the only one saying it. In any case, others are posting more reasonably that we're on track for a very similar season to last year when most people also skipped on the snow gear. We still have a couple more weeks till we're anywhere close to the big mountains. A late-May snowstorm seems pretty highly unlikely, and I expect what's there will just keep melting.

We also talked a bit about the annual ALDHA-West get together. Dragonfly & Beaker have been a couple times & say it's lots of fun. Our plan is to finish well ahead of the ALDHA-West dates in late September, so we keep this in mind for the rest of the trail, maybe trying to make it on our way back south (though it doesn't happen in the end).

[![The trailer where we stayed at the Saufley's](http://farm6.staticflickr.com/5487/11143541716_bd1df67f31_b.jpg)](http://www.flickr.com/photos/_ellie_/11143541716)

After breakfast, we came back to Hiker Heaven and moved out of our room to let the couple from Portland take it over -- Rocky & T-Rex. They managed to get here early enough to not have to spend a night at the goat farm first!

It seemed like all the various helpers at the Saufley's didn't want us to do *any* work, but after some convincing we were allowed to change the sheets on the bed ourselves, and clean up the room a bit. We failed to find a vacuum, but at least picked up the miscellaneous bits of straw and such from the floor.

After cleaning up and getting our stuff out of the way, we spent the rest of the morning relaxing and catching up with folks. Another blissful day under the shade of the giant tree in the Saufley's yard.

[![Here's Luke & MeHap listening to Crash tell some kind of totally crazy story about nearly dying. Apparently he's a magnet for crazy trips and falls, earning him the name.](http://farm6.staticflickr.com/5509/11136586365_656735ec4d_h.jpg)](http://www.flickr.com/photos/_ellie_/11136586365/)

[![And here's Dragonfly & Beaker soaking their feet. I managed to fit in another foot-bath for myself today, too! The extra day of rest has really been good for the blistery toes I think.](http://farm3.staticflickr.com/2890/11136758503_317225156e_b.jpg)](http://www.flickr.com/photos/_ellie_/11136758503/in/photostream/)

We had a good conversation with Dragonfly & Beaker & Beads about trail maintenance -- something everyone seems to have an opinion on, CONSTANTLY. I forget exactly how it came up this time around, but the conclusion was that everyone who's out here hiking should have to do trail maintenance at least once.

Luke and I recounted going out at kickoff last year for a short trail maintenance day near Lake Morena, but didn't make it out again this year. It seems like this could be better promoted at kickoff -- or perhaps scheduled for a day in advance of the main festivities. Especially because current year thru-hikers, it's hard to make the decision to skip the water report presentation, or Kolby's cool journaling talk, and such to leave for an entire day.

Beads and I both recalled [Bob Peoples](http://www.blueridgeoutdoors.com/special-sections/appalachian-trail-guide/bob-peoples-trail-magician/) from the AT with much fondness. He manages to organize quite the crew each year for the annual Kincora [Hard Core](http://tehcc.org/2013/07/2013-damascus-hard-core/) maintenance party that takes place on the two days after the AT [Trail Days](http://www.traildays.us/) celebration. And, as Beads recalled from her NoBo AT hike, he also gets such a huge percentage of NoBo thru-hikers to take a zero at his hostel and do some trail work then.

Beaker pointed out that he didn't think that'd work so well on the PCT -- convincing people to zero and participate in trail work early in the trail, because on the PCT the time constraints are so different. On the AT, one's schedule can eel pretty flexible -- start sometime between February and May; finish sometime between August and October.  The west coast weather, and higher elevation mountains, make the PCT a little less forgiving. Almost everyone starts in late April, and a safe finish date, if you want to avoid seeing any snow in WA, seems to be mid-September. This year the weather was certainly amenable to a somewhat earlier start, but I'm still feeling pretty good about ours.

About the scheduling thing, Beaker says "some people figure it out sooner, others later." I feel like Luke and I are on a fine track to make it by our mid-September target, though we certainly aren't as rushed as some people, and we started a lot slower for the first couple of weeks especially. But, I spent SO MUCH TIME with [OTC's trail planner](http://www.pctplanner.com/) before we left -- I know the math works out that we'll get there even if we don't bust out 20+ every single day.

Another couple we met a few days ago, but whose names I forget... but who I remember live in the Bay, too -- near Lake Merritt -- also sat down and chatted for a while about just how DIRTY we all have been getting in this dusty desert land. The woman told us that she actually resorted to dying her shirt blue at a recent town stop because she couldn't keep it clean and it was bothering her so much! I prefer to think of the dirt that has found its way into the unwashable core of my once-white rail-riders shirt as more of a badge of honor ;)

[![A couple sorting their resupply package on the Saufley's re-opened lawn](http://farm8.staticflickr.com/7349/11136579945_15aaaa8d41_c.jpg)](http://www.flickr.com/photos/_ellie_/11136579945/)

While chatting, we all attached new velcro to our newly-purchased shoes from the REI trip yesterday so that we can keep wearing our gaiters -- so exciting to have new shoes! And then Luke and I finally decided we should tie up a few chores before maybe thinking about heading back to the trail. We have surrendered our sleeping spot to the couple from Portland, so we can't change our minds and double-zero now! We also still haven't gone to the store and shopped for our resupply!

[![The internet tent in the Saufley's driveway](http://farm4.staticflickr.com/3758/11136575835_1948737c3a_b.jpg)](http://www.flickr.com/photos/_ellie_/11136575835/)

I wandered off to try and update the blog at the set of laptops, although I quickly became frustrated trying to deal with getting pictures off my phone and onto wordpress, as well as writing on the laptops.

[![Just like the table at the Warner Springs community center, there's a shelf in one of the RVs here overflowing with cameras and phones and iPods charging off of a whole mess of power strips.](http://farm3.staticflickr.com/2853/11136747053_f728d876c3_b.jpg)](http://www.flickr.com/photos/_ellie_/11136747053/)

Today is Lilly's defense. I sent her a good luck text early in the morning, and then, while at the laptops I saw the email on LUCI mailing list congratulating her on passing, of course. So I sent her a congrats text, too. She replies, 'so much for disconnection' or something. But, I think this is the perfect disconnection. No one else has any expectations of me. I can still choose to tap into the Saufley's WiFi while i'm here, and check on what's happening with her. But. I don't have any accountability for it. Still in Southern California, where cell or internet service is at least fairly frequent, and when we come into town, it's no problem; I feel like I have a lot of control over what's going on -- I can choose to connect when something important is happening, and to ignore all the stuff that doesn't really matter to me.

[![Meanwhile, Luke worked on fixing some flats & replacing tires on some of the bikes. ](http://farm3.staticflickr.com/2834/11136598836_7b85bdb107_b.jpg)](http://www.flickr.com/photos/_ellie_/11136598836/)

[![The hiker boxes at the Saufley's are no joke! They are SO organized, in big rubbermaid tubs with lids & bricks to hold them in place in the wind.](http://farm6.staticflickr.com/5521/11136565975_7cb311fb98_c.jpg)](http://www.flickr.com/photos/_ellie_/11136565975)

[![Another hiker box shot](http://farm8.staticflickr.com/7402/11136734403_06f3cdbbd0_c.jpg)](http://www.flickr.com/photos/_ellie_/11136734403/)

[![Starbucks donated thousands of VIA instant coffee packets to trail angels all along the PCT. These were the first ones we ran into. I guess we were supposed to be #hashtagging about coffee while we hiked?](http://farm8.staticflickr.com/7416/11136624914_1bba26992a_c.jpg)](http://www.flickr.com/photos/_ellie_/11136624914/)

[![I donated my old shoes to this box after upgrading to the larger size at REI yesterday. Our friend Sagitar ends up walking off with them. He has a new pair coming at Kennedy Meadows so he doesn't want to buy brand new ones at REI here, but also doesn't want to struggle with his old shoes any longer! It works out well for both of us :)](http://farm4.staticflickr.com/3768/11136563615_f3f02d7793_c.jpg)](http://www.flickr.com/photos/_ellie_/11136563615/)

[![We start getting serious about leaving and go up to the garage to say goodbye. Egg, Ole, and MeHap are just arriving to settle down for their zero day.](http://farm3.staticflickr.com/2851/11136618574_55f888acb9_c.jpg)](http://www.flickr.com/photos/_ellie_/11136618574)

We find Donna for goodbye hugs -- it's **so** amazing to meet strangers one day and then have them treat you like their child 24 hours later -- get our picture taken, sign our names and email in the official hiker register that will be used for making the class video at the end of the trail, and struggle to make a donation to this very generous trail angel. I realize as we're heading off, hugs and photos taken that I still haven't found the donation box, and I'm feeling really awkward about asking, but this place has been amazing, and I know all of these overflowing hikers cost money -- money for water, electricity, tents, hiker boxes, the van to REI and so much more. So, I get up the courage to ask about it, and Donna says oh no you don't have to leave a donation. She don't want people to feel like they have to leave money to stay here. But, I persist and she explains that yeah if she doesn't designate a space, then people leave money randomly all over, which can be a problem, too. So, she directs me to a little pink flower pot hidden above the laundry machine. I can leave money there, and so I do.

We make the long walk back to town, head to the grocery store to finally do our resupply shopping, and then, of course, make one final stop for a late lunch of PIZZA AND BEER!

[![This place was one of our favorite pizza stops on the whole trial. So yummmmm.](http://farm4.staticflickr.com/3670/11136615244_30b1be1e49_b.jpg)](http://www.flickr.com/photos/_ellie_/11136615244/)

We run into Dragonfly and Beaker again at the pizza place and share another meal with them. They've also just finished their resupply shopping -- they are actually managing to get work done on their zero day and are aiming to leave the Saufley's early tomorrow morning. I'm a little impressed with their motivation! And also glad to know they won't be far behind us even though we're getting out of town a day ahead of them.

We share our tip on where to find the donation jar at the Saufleys. They, too, feel compelled to leave a donation, and find it frustrating that the location would be hidden away. We can all appreciate the place of generosity where Donna's decision to hide the jar is coming from. But, as Beaker points out, being one of the first big trail angels on the PCT, it seems to set a precedent for hikers that donations aren't necessary or expected. While the Saufleys might be doing well enough financially to bankroll the whole show, not all trail angels are in such a position -- especially as the number of hikers and their impact during thru-hiker season grows -- and it seems like it might be good for hikers to just get in the habit of contributing early on.

Veggie & Spark are also at the Pizza place and we chat with them briefly. When we leave (around 4pm!!), MeHap is outside on the corner looking for them and trying to call them, but they never connect. They left the pizza place ahead of us, and we weren't sure if they were headed straight back to the trail or what, so we're not much help :( We also run into Wild Child and Seminole packing up beers for the 24 miles / 24 hours / 24 beers challenge that some of the hikers try to do between the Saufleys and the next big trail angel house, Casa de Luna / the Andersons. Luke and I are not so into the idea of getting drunk in the desert with unreliable water sources... so we pass on joining. Also, apparently we are old curmudgeonly grownups these days, I suppose. We're thankful that Wild Child decided to 'personalize' the challenge, by cutting it down to only 18 beers in 24 hours. And, heading out in the evening should at least mean for nice cool hiking for them. (FWIW, they will, in fact, make it to the Andersons totally fine the next day, just a couple hours after us.)

[![Local lawn art on the way out of town! Pretty sweet :)](http://farm3.staticflickr.com/2867/11136542605_7f97a264c0_c.jpg)](http://www.flickr.com/photos/_ellie_/11136542605)

[![Luke crossing the road as we get out of town](http://farm8.staticflickr.com/7299/11136608544_f630ab4a73_c.jpg)](http://www.flickr.com/photos/_ellie_/11136608544/)

And, so, we eventually make it back to the trail, hiking along the road out of town, and then heading up into beautiful golden hills just as the sun is getting low in the sky.

[![Lovely glowing grasslands](http://farm8.staticflickr.com/7434/11136706053_4040522533_c.jpg)](http://www.flickr.com/photos/_ellie_/11136706053)

[![I was falling behind with all my picture taking, and feeling grumbly about hiking on my beaten up feet again. Luke left me this 'woo woo' note in the trail.](http://farm8.staticflickr.com/7427/11136527395_a2a3c368e4_c.jpg)](http://www.flickr.com/photos/_ellie_/11136527395/)

[![Luke hiking in the golden hills](http://farm8.staticflickr.com/7435/11136553226_7a407fc419_c.jpg)](http://www.flickr.com/photos/_ellie_/11136553226)

[![Self-portrait in the evening sun](http://farm3.staticflickr.com/2848/11136519215_97136ea96c_c.jpg)](http://www.flickr.com/photos/_ellie_/11136519215)

The hike up from Agua Dulce back to the crest is rumored to be one of the hotter hikes on the trail, but the evening weather is lovely for it. It's breezy and even a little bit cloudy. I even start to get chilly before we stop for the night.

Our big goal was just to get back to the trail, so we don't hike terribly long even given the late start. We also know that we were only 24 miles out from the Anderson's when we left town, and so really, any mileage today is just about getting us in to Green Valley in time for lunch as opposed to dinner. Around 7:30, after about 10 miles, we find a suitable campsite tucked away in some bushes on the top of a hill, and set up camp as the sun goes down.

[![PIZZA!](http://farm4.staticflickr.com/3722/11136690013_24cf162504_c.jpg)](http://www.flickr.com/photos/_ellie_/11136690013/)

We feast inside the tent on leftover pizza from lunch, some broccoli/cheese/noodle instant dinner, some wine we packed out from town, and Oreos. Luke, still fighting to keep on the weight, added a snickers to all that!
