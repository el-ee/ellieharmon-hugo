---
author: ellie
comments: true
date: 2013-05-08
layout: post
slug: day-18-idyllwild-to-197
title: 'Day 18: idyllwild to ~197'
wordpress_id: 2143
tags:
- PCT
---

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_081402.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_081402.jpg)

Got a ride along with lots of other hikers from the hotel folks up to the devils slide trail head at eight am. Look at that pretty sunshine coming out!

<!-- more -->

Nice hike up devils slide -- great morning warm up! And we needed it after the freeeeezing ride in the pickup! There was just a tiny bit of snow left up on the mountain so we had a very pretty - and also quite easy - hike all the way through Forrester pass.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_125624.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_125624.jpg)

We ran into mehap again, who we had originally met at kickoff when we stopped for water at a lovely creek that was just gushing with clear cold liquid! We haven't seen much of this so far in the desert! Wild child said it reminded her of the sierras! No doubt.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_150519.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_150519.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_152140.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_152140.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_165925.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_165925.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_192212.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_192212.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_191213.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130508_191213.jpg)

Ended up camping back down in the desert near wild child and mehap both. Had a long and great convo with mehap after dinner while watching the lights of the wind farms and some kind of cities still further down the hill below us. It's strange how the lights make things visible at night that you could never see in the daytime. We were all shocked when we stood up after talking for a while to see so many signs of cities on the desert floor that had been invisible before.

Mehap, like luke, is from Pennsylvania and, like me, did the AT before coming out west for this trail. We talked a long time about work and jobs and how to fit something like a six month hike into all that.. And how a long hike teaches you different things about what it means to live 'comfortably' -- that comfort isn't about having lots of things and working all the time. That one of the things to take back with you from a long hike is how to live smaller .. that you don't need that stuff (big house two cars three kids) that society says you need to be happy. I think we all three share a real fear of getting 'stuck' in a job, tied to a mortgage, etc.
