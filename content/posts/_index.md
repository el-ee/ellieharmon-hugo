---
title: "Thinking Out Loud"
date: 2019-02-09T15:49:00-08:00

---

![PCT journaling](/images/PCT-writing.jpg)

Mostly bits of my journal from my 2013 PCT hike. Occasionally, other thoughts.
