---
author: ellie
comments: true
date: 2013-04-28
layout: post
slug: day-8-1-week-on-the-trail
title: 'Day 8: 1 week on the trail!'
wordpress_id: 2063
tags:
- PCT
---

Hiked out from Mt. Laguna today and camped out at a lovely boulder field -- our best campsite of the trail yet!

While at kickoff, got the brilliant idea from [Robin](http://somanymiles.wordpress.com/) and [Rachel](http:// irunhikecamp.wordpress.com/) to do a weekly video chat with you all... so, instead of a blog post, here's a little movie!

[embed]https://www.youtube.com/watch?v=7DpU97ris7s[/embed]

Cheers to Luke for getting it from the GoPro to his tablet and then uploaded to YouTube :)
