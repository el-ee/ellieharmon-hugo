---
author: ellie
comments: true
date: 2013-05-16
layout: post
slug: day-26-hillside-site-to-gobblers-knob
title: 'Day 26: hillside site to gobbler''s knob'
wordpress_id: 2280
tags:
- PCT
---

We woke up a little before sunrise this morning, on a mission to make it to the McDonald's at Cajon Pass before they stopped serving their breakfast menu. We were only about 8 miles out, but we weren't really sure what time it changed over, and definitely didn't want to miss an opportunity for biscuits and hashbrowns!

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_051931.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_051931.jpg)



<!-- more -->


Shark Bite and Faucet were camped less than half a mile behind us, and they bounded by while we were still packing up, taunting our always slow-to-get-started selves, "see you at breakfast!"

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_074033.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_074033.jpg)


There was CRAZY fog all morning, almost misting on us through this 'desert' section of trail. As usual, we spent a while walking under some high-voltage power lines (i suppose land that no one wants is easier to commandeer for hiking trails/maybe the government already has an easement there? Though I think southern California Edison is private?). In the fog, though, we couldn't quite see them! But we could hear their crackle, so I knew they were close. And the trail was road-like, you know like power-line-access-road-like. And, when we got close to a tower, which we could see materialize out of nothing as we got closer, the electricity (? or something?) seemed to condense the fog. Little raindrops pelted down in a little circle around it. Creepy!

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_075515.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_075515.jpg)

Also as usual, we saw lots of barbed wire disappearing off into the fog. Or just disappearing after only a couple feet. Strange! I don't understand all the boundaries on this trail. I assume most of the barbed wire is related somehow to cattle grazing. Though maybe in some places it's just being used to deter motorbikes/ATVs? I find these short stretches of it especially perplexing.

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_085529.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_085529.jpg)



The fog finally cleared as we made our way down to a TINY little creek -- our ticket to get under the big freeway via a drainage culvert. But first. McDonald's.

We ran into cuddles at the trail crossing coming back from a cello gig. He said the McDonalds record was 5000 calories -- but not by him. He only managed 4000.

[caption id="" align="alignnone" width="500" caption="Luke makes a dr pepper float (McDonald's doesn't sell root beer)"][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_102934.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_102934.jpg)[/caption]


[caption id="" align="alignnone" width="500" caption="Shark bite and faucet make a float, too. They are from Corvallis and know Camille's mom from the farmers market!!"][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_103310.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_103310.jpg)[/caption]


We easily made it in time for breakfast. And, after a couple biscuits, hashbrowns, and coffees... went back for ice cream. Luke opted for a DIY Dr. Pepper float (which totally started a trend), and I went for an Oreo McFlurry. Normally, we are NOT McDonald's people. But, man, that high-fat, high-salt breakfast was SOOO delicious to our thru-hiker selves.

We didn't end up doing all the math on our calorie counts (CA law requires that restaurants display this info on the menu) but I think we were probably only around the 3000 range.

[caption id="" align="alignnone" width="500" caption="Our friends egg and ole. Ole turned 25 today! Happy birthday :)"][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_103423.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_103423.jpg)[/caption]



[caption id="" align="alignnone" width="500" caption="Taking over the patio"][![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_104107.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_104107.jpg)[/caption]


The McDonald's employees were pretty chill, as the thru-hikers all took over the outdoor patio, even hanging out tents and sleeping bags on the rails to dry in the sunshine -- everyone got a little unexpected condensation overnight. I think everyone enjoyed the break and proximity to the freeway meant we all had cell service for updating blogs, posting instagram pictures, and conversations like:
--"are you trusting the caches?"
--[pulls up pct 2013 fb page] "yesterday it was refilled to 90 gallons"

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_115806.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_115806.jpg)


After our McDonald's detour, we headed back up to the mountains; going under the freeway via the drainage culvert, and then under and over a set of three heavily used railroad tracks.

[![image](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_171311.jpg)](http://ellieharmon.com/wp-content/uploads/wpid-IMG_20130516_171311.jpg)


We have seen lots of red velvet looking ants (?), but this was our first yellow one. Not sure what the story is with these bugs, but they sure are neat looking!

We did our first road walking today, opting to take a jeep road to avoid some poodle dog bush that is rumored to be difficult to avoid in a short section of trail this afternoon. The jeep road basically paralleled the trail a little higher up on the ridgeline. We enjoyed the views a lot and the ability to walk beside each other and talk. Though it was a bit steep on some of the climbs and exposed, we have no regrets on the detour.

At night, we caught up to toots and tears at our campsite as well as seeing shark bite and faucet again. We enjoyed chatting with them all over dinner -- and we had cell service from the mountain so I totally killed my battery sending my mom silly pictures of luke eating dinner & making sure she was doing alright in SF. Which, of course, she was.

We are really looking forward to showers and laundry in wright wood. AND getting new shoes in agua dulce -- both of us underestimated just how much our feet would expand and are starting to get some annoying blisters.
