---
author: ellie
comments: true
date: 2013-05-03
layout: post
slug: day-13-tule-spring-to-some-boulders
title: Day 13 - tule spring to some boulders
wordpress_id: 2122
tags:
- PCT
---

Other hikers will find this absurd. My mom will probably appreciate: Our earliest start yet! We were walking by 740am!

[caption id="" align="alignnone" width="500" caption="Luke walking"][![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130503_130934.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130503_130934.jpg)[/caption]

<!-- more -->[caption id="" align="alignnone" width="500" caption="Some kind of water storage in the desert. We didnt stop here and i forget the history of this crumbling reservoir"][![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130503_083705.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130503_083705.jpg)[/caption]

Perhaps motivated by the promise of multi-ingredient hot food items at paradise cafe (and easy to access water from a garden hose) we hiked pretty fast in the morning. When not PCT season, it's a biker bar near Anza, CA best I can tell.

As we approached the junction to the one mile paradise cafe side trail, we noticed a shade tent set up ahead by the road and decided to check it out. There was Dr. Sole working on peoples blisters and giving out sodas! Also, the paradise cafe owner was hanging out giving people rides a mile up the highway to his place!

We jumped for a ride and had a great jose burger (jalapeno bacon cheese mushrooms avocado lettuce tomatoes pickles) and some very tasty cold beers on the patio and chatted with some hikers we haven't met before.

As we were leaving some hikers came by to refill the jugs being used for a small water cache back at the trail crossing. I helped them get a ride for the water back to the trail with my magic female hitchhiker skills, though luke and I ended up walking back - not enough room for everyone in the car.

I was thinking about getting my blister looked at by the doc (actually a retired truck driver), but its not looking or feeling too bad. And gummie bear was in the chair like over an hour getting new feet and we were antsy to walk more.

We found a great campsite a little before we had planned to stop and couldn't pass it up. Nice big boulder outcropping looking west towards the sunset with a flat sandy area perfect for pur tent tucked into a corner by some junipers. Sadly I failed to bring my phone charger to the cafe so the battery was dead and all the lovely sunset pics are stuck on the real camera. Check back in October or so...

We are looking forward to a zero in idyllwild on Monday! Hadn't originally planned to stop but we are ahead of schedule and would like to give the blister some time to heal + we hear its a super friendly town and there are rumors of bad weather on the way.
