---
author: ellie
comments: true
date: 2013-05-09
layout: post
slug: day-19-197-to-whitewater-preserve
title: 'Day 19: ~197 to Whitewater Preserve'
wordpress_id: 2163
tags:
- PCT
---

[caption id="" align="alignnone" width="500"][![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_061204.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_061204.jpg) Sunrise over the desert[/caption]

What a day! Crossed the 200 mile mark, crossed under I-10, stopped by the amazing Ziggy and the Bear's, and ended the day with bobcat AND desert bighorn sightings at Whitewater Preserve!

<!-- more -->

[caption id="" align="alignnone" width="500"][![Passing the 200 mile marker on the way down!](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_082554.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_082554.jpg) Passing the 200 mile marker on the way down![/caption]

Long descent this morning all the way down to the desert floor; I was really thankful my knee didn't give me too much trouble! Yay! We passed the 200 mile marker early in the morning.... and then passed like three more 200 mile signs. Clearly people have some different ideas about just where the right spot is. The official PCT 200 mile marker was a mile or so late, showing it's age, and evidence of the trails constant movement.

[caption id="" align="alignnone" width="500"][![Walking along a fence, a common activity... seems that we are always at the edge of some place we're allowed (or not allowed) to be](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_124048.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_124048.jpg) Walking along a fence, a common activity... seems that we are always at the edge of some place we're allowed (or not allowed) to be[/caption]

[caption id="" align="alignnone" width="500"][![Powerlines fed by the big wind farms](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_111745.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_111745.jpg) Powerlines fed by the big wind farms[/caption]

We spent a long time walking under the big high voltage powerlines near Cabazon with the big windmill farms that we always see when we drive out to Joshua Tree in the background. In addition to their usual electric crackling thing, they were under construction, with helicopters flying around and lots of slack in the lines that made them sing (kindof like how it sounds when someone is playing a handsaw) and snap.

[caption id="" align="alignnone" width="500"][![Rainbow over the wind farms as we climb up out of the valley](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_172810.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_172810.jpg) Rainbow over the wind farms as we climb up out of the valley[/caption]

Later in the day, after Ziggy and the Bear's, we actually walked through the Mesa Wind Farm, the wind singing through the cage bases of them sounded like ghosts. Dark nearly rainy skies made it quite eerie! Though we did get a pretty rainbow as we climbed the hill past them.

Thinking a lot about all this infrastructure for civilization today. We're getting quite the tour of it out here -- powerlines and wind farms; a big desert water authority pipe this morning (monitored with an HD IR camera); we'll hit several Southern California Edison lakes soon. Walking through it all, though, we have access to none of it. Carrying our own little mini solar panel to recharge phones and cameras; drinking lots of water from caches trucked out by trail angels hundreds of gallons at a time.

[caption id="" align="alignnone" width="500"][![The long sandy approach to I-10](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_114806.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_114806.jpg) The long sandy walk up to I-10[/caption]

[caption id="" align="alignnone" width="500"][![Luke finally approaches I-10](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_115116.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_115116.jpg) Luke finally approaches I-10[/caption]

![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_121105.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_121116.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_121116.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_121111.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_121111.jpg)

We walked in sand for what seemed like FOREVER to get to I-10, but, once arriving, had quite the lovely surprise. Some hikers from last year had set out several coolers of beer and sodas in the shade of the railroad bridge. We sat down with Mehap and a father-son pair of section hikers, Jon and Ben, for a refreshing mid-day break.

[caption id="" align="alignnone" width="500"][![Luke and Mehap enjoy a refreshing beverage under the train tracks by I-10](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_115836.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_115836.jpg) Luke and Mehap enjoy a refreshing beverage under the train tracks by I-10[/caption]

[caption id="" align="alignnone" width="500"][![A little sign pointing us to Ziggy & the Bear's](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_124747.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_124747.jpg) A little sign pointing us to Ziggy & the Bear's[/caption]

Not too far after the freeway crossing, we started seeing signs for Ziggy and the Bear. Almost as soon as we got in the door, Cool Ranch & Capitan had warm salt baths to soak our feet! These guys are two hikers from last year who are hanging out at trail angel houses this summer helping out because they said that their own hikes last year were so euphoric, they felt they had to do something to return the kindness they experienced.

Another volunteer made an extra Burger King run just for the three of us (we arrived right as the first one was being delivered to everyone else) to pick up a high-calorie lunch for us!

[caption id="" align="alignnone" width="500"][![Mehap and Wild Child chilling at Ziggy and the Bears](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_150614.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_150614.jpg) Mehap and Wild Child chilling at Ziggy and the Bears[/caption]

[caption id="" align="alignnone" width="500"][![Luke does some duct tape sewing to patch a hole in his shoe](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_1406321.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_1406321.jpg) Luke does some duct tape sewing to patch a hole in his shoe[/caption]

We decided not to spend the night, but really enjoyed hanging out for a few hours, talking with folks about hiking and the like. Several people sharing tips about how they make their hiking fit into their life/how they afford it -- bar-tending at ski resorts is a pretty popular winter job. One lady even has a 5 year plan to hike the AT, PCT, and CDT. She did the AT in 2011, split summers in a national park up north (maybe Glacier, I forget?) and winters at a ski resort. She's doing the PCT this year. And plans to do the CDT in 2015.

Cool Ranch was quick on the experienced hiker advice, especially to one person who was trying to hurry to catch up and meet someone who was further up the trail. Seems a common activity, and one that's often anxiety inducing! Cool Ranch's opinion was that trying to meet people or coordinate schedules will "ruin your hike"!

There was also a bit of AT dissing -- which I've heard a lot out here -- people saying that they only want to do the PCT or the CDT because the AT is too much of a party trail or there are too many towns and roads near it or that it's too 'easy' or blah blah blah. Beads (who we have been running into since day 1 -- she took our picture at the monument!) was doing a pretty good job of AT defense. I, myself, am constantly surprised by just how many roads and fences and powerlines and etc. are out here. I was EXPECTING the PCT to be more 'remote' more 'wild' and all those other things people say. It's definitely a lot more crowded than I expected, but I think that trend has been going up for a few years now. And, so far, seems like SoCal, at least, is pretty similar to the AT. Maybe even less remote feeling. Maybe it is just because of the desert/flatness/lack of trees, but I feel like we can see a nearby road much more of the time than not! And, while the trail itself doesn't go directly through many towns, it's not like all the hikers aren't going to those towns. So far, lots of resort-y places -- Idyllwild, Big Bear... rather more enjoy the old industry towns of the east coast. Different kinds of people that you meet there. Also, airplanes. SO LOUD. And EVERYWHERE. Were talking with Mehap about this last night -- even when we are far from a road, it seems that we're never not underneath a flight path.

[caption id="" align="alignnone" width="500"][![Egg & Ole walk through the amazing trails at Whitewater Preserve](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_184914.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_184914.jpg) Egg & Ole walk through the amazing trails at Whitewater Preserve[/caption]

[caption id="" align="alignnone" width="500"][![So much water, there's a bridge to cross it!!](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_185009.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130509_185009.jpg) So much water, there's a bridge to cross it!![/caption]

In any case, we ended the day with a nice late afternoon/early evening hike to Whitewater Preserve. We really enjoyed the cloud cover on the walk back up at least partly out of the desert. Whitewater Preserve turned out to be a super cool place -- a trout farm recently turned into a privately managed conservation area. It is normally a picnic area, but they let thru-hikers camp overnight when they're closed. We saw our first big mammals! A bobcat in the grass and some desert bighorns up on the rocky hillside above where we camped!!

We got a little rain on our tent when we first arrived -- the first rain we've really encountered -- but we had a picnic shelter nearby convenient for cooking and eating dinner, and it cleared up pretty quick, so we didn't get too wet ourselves.
