---
author: ellie
comments: true
date: 2013-05-15
layout: post
slug: day-25-deep-creek-footbridge-to-hillside-333
title: 'Day 25: Deep Creek Footbridge to Hillside ~333'
wordpress_id: 2259
tags:
- PCT
---

[embed]http://www.youtube.com/watch?v=JLjD6CYnwjc[/embed]

This climb is how we started our morning.

<!-- more -->

Looking back from the footbridge, after we got up to the trail:

[caption id="attachment_2260" align="aligncenter" width="525"][![Looking back at our campsite as Egg & Ole pack up](http://ellieharmon.com/wp-content/uploads/IMG_20130515_072916-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130515_072916.jpg) Looking back at our campsite as Egg & Ole pack up[/caption]

[caption id="attachment_2261" align="aligncenter" width="525"][![Looking the other direction up the creek](http://ellieharmon.com/wp-content/uploads/IMG_20130515_072930-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130515_072930.jpg) Looking the other direction up the creek[/caption]

Today ended up being another 24 mile day, brought on by a search for water + somewhere FLAT. The two seem to be few and far between out here. What is not few and far between, however, is trail magic! IT IS EVERYWHERE!

Our morning started out with a hike out to the Mojave dam, which seemed to be dam-ing up nothing. Dry as a bone on both sides! OK, there was a TINY bit of water in deep creek near the dam, which we 'forded,' but even that was dried up after a half mile or so farther.

[![IMG_20130515_085222](http://ellieharmon.com/wp-content/uploads/IMG_20130515_085222-768x1024.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130515_085222.jpg)

We later learned from some locals, that I guess it is a dam to prevent flooding from the deep creek side, which I can't even imagine, but supposedly it happens once a decade or so.

[caption][![IMG_20130515_092840](http://ellieharmon.com/wp-content/uploads/IMG_20130515_092840-768x1024.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130515_092840.jpg)Morning trail magic! Luke got a root beer float instead, because he is crazy. This orange one was AWESOME.[/caption]

These locals were hanging out at the road crossing just a few miles into our hike handing out ORANGE SODA FLOATS! OMG! Along with danishes and hot dogs! With chairs! In the shade!

[caption id="attachment_2265" align="aligncenter" width="525"][![Hikers chilling in the shade. In chairs!](http://ellieharmon.com/wp-content/uploads/IMG_20130515_094837-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130515_094837.jpg) Hikers chilling in the shade. In chairs![/caption]

There is some talk for a bit about how the trail angels ID thru-hikers versus regular day hikers -- by their dirtiness. The soda-bringers joked that they considered whether they should stop using deodorant and drier sheets for a few days before setting up their regular station here. Wouldn't want to make the thru-hikers smell THEM!

They say that we're the biggest crowd they've seen yet, but that this year the shape of hikers coming through has been different. Lots started early, and so there was a steady but slow stream for several weeks. Now we seem to be at the front of the big 'herd' -- the leading edge just before the peak of a bell curve.

[caption id="attachment_2267" align="aligncenter" width="525"][![Lunchtime under the bridge](http://ellieharmon.com/wp-content/uploads/IMG_20130515_135544-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130515_135544.jpg) Lunchtime under the bridge[/caption]

[![IMG_20130515_135549](http://ellieharmon.com/wp-content/uploads/IMG_20130515_135549-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130515_135549.jpg)

We hiked on and on in the hot sun, misguided by a misplaced note in our Halfmile app, thinking we were going to arrive at a big lake ANY MOMENT NOW. Several hours later, we finally arrive on the wrong side of the dam to enjoy the lake, and huddle with other hikers under a tiny bridge to catch some lunchtime shade.

[caption id="attachment_2275" align="aligncenter" width="525"][![Hiker footprints & MCI Fiber](http://ellieharmon.com/wp-content/uploads/IMG_20130515_144329-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130515_144329.jpg) Hiker footprints & MCI Fiber[/caption]

Walking around the dam, we were constantly on the 'wrong' side of lots of 'no tresspassing' signs. The PCT in SoCal is routed on so much random government land -- and we're getting quite the tour of water, electric infrastructure for the big cities farther west. I feel like anytime we see a powerline or, in this case, some weird storage yard for reservoir related piping, that's where the PCT goes.

[caption id="attachment_2274" align="aligncenter" width="525"][![Spillway](http://ellieharmon.com/wp-content/uploads/IMG_20130515_143334-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130515_143334.jpg) Spillway[/caption]

Just after passing the spillway for the dam, we re-entered some trees and found a cooler hiding in the shade with water, fresh fruit, and hard boiled eggs! Awesome unexpected treat #2 of the day!

[![IMG_20130515_161705](http://ellieharmon.com/wp-content/uploads/IMG_20130515_161705-1024x768.jpg)](http://ellieharmon.com/wp-content/uploads/IMG_20130515_161705.jpg)

And then, we did, EVENTUALLY, get to the lake lake, and after torturously walking around its edges for what seemed an eternity, we took a side trail down to a "boat in" picnic area, and I dove in and did some REAL swimming for the first time on this trip. It felt AMAZING.

A few miles later, we came to another picnic area that had real bathrooms with flush toilets and clean running water right out of a faucet. I'm tempted to call this trail magic #3 of the day, but more like a run in with regular civilization, I suppose.

Just past the picnic area, as we headed up into the hills for our last climb of the day, we crossed a freeway where someone was scouting support stops for a 100 mile run they're planning on Saturday. This was trail magic #3 -- a handful of delightfully salty pistachios :)

We made it up into the hills and found ourselves a campsite about a mile past our planned final water-stop for the day. We are close to Cajon Pass, where we'll cross I-15 (at a McDonald's, which we plan to hit up for breakfast!) We can hear the low roar of cars, planes and loud trains from our campsite. There are lights everywhere -- a truck driving on a jeep road up above us, cars below in the valley, towers with flashing red lights.

There is some kind of weird animal outside our tent at night. We never do figure out what it is. While I wrote about the day in my journal, we heard it crunching around in the bushes, but no matter how much noise we made, it wouldn't go away. We couldn't see it with our lights, and it never came that close -- seemed totally uninterested in us, but like it was walking around in circles in the bushes, rummaging for something. But, I still thought maybe I wanted to have the fly on the tent as dumb as that sounds. As if that thin piece of opaque nylon would protect us better than the tent's more transparent mesh...
