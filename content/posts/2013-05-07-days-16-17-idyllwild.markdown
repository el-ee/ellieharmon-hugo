---
author: ellie
comments: true
date: 2013-05-07
layout: post
slug: days-16-17-idyllwild
title: 'Days 16-17: idyllwild'
wordpress_id: 2134
tags:
- PCT
---

[caption id="" align="alignnone" width="500" caption="Cuddles plays the cello at cafe aroma"][![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130505_125423.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130505_125423.jpg)[/caption]

Yesterday, on our way down the mountain on the devils slide trail around 11, we ran into several hikers on their way back from idyllwild. More than one said that [cuddles](http://www.trailjournals.com/markvpct13/) was in town playing his cello at a cafe at 1 and we shouldn't miss it! We managed to get a super fast ride from someone hanging out in the parking lot just looking to help hikers out! He dropped us at the idyllwild inn where they set us up who an AWESOME cabin for a sweet price and we made it to the cafe in time to get a front row seat for the concert. Had a great time listening to the music, chatting with cuddles, and eating delicious brunch food complete with a bloody Mary! We capped it off with the most amazing caramel cake and a jewbilation sweet 16. Yum!
<!-- more -->[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130507_194448.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130507_194448.jpg)

We ended up spending two zeros in town to avoid the big storm that came through the mountains and let our blisters and my knee have some down time for a couple days.

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130507_132657.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130507_132657.jpg)

Spent a fair amount of time at the library trying to take advantage of a full size keyboard to catch up on these blog posts! Such a hiker friendly town: they had water reports already printed out and ready for folks to pick up! On our way to breakfast, a member of the american legion thanked us for coming into town and, as if it wasn't obvious, told us we're very welcome here! I was like, wait, you're thanking me? This place is great and your fancy cafe didn't balk at my not yet showered and fresh off the trail self! Thank YOU!

Other highlights revolve around people and food:

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130505_192311.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130505_192311.jpg)

We had mexican food and tasty (and huge) micheladas with [the Louisiana boys](http://swamptosierras.tumblr.com) for cinco de mayo.

Met lots of people at the pizza shop and the rad outfitter: wren and bagpipe, pickle and lucky Irishman. We caught up with first light again. As well as dragon fly and beaker. And finally chatted a bit with hummingbird, who we had been hiking back and forth with for several days.

And met a new friend, sour cream, in the coffee shop (which we also really enjoyed. Fresh roasted beans! What a treat!)

[![image](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130506_092140.jpg)](http://ellieharmon.com/wp-content/uploads/2013/05/wpid-IMG_20130506_092140.jpg)

The inn had to move us to a different location for our second night because, as the woman in charge put it, some "I don't want to say normal, but non-hikers" had our cabin reserved. This resulted in the occasion to meet a whole new crew of folks who were staying next door and kindly let us crash their cabin for the afternoon in between check out and check back in time: [scooter, boulder](http://jenandjoey.tumblr.com), sweet tooth, dance party, sunshine, and buffalo. Scooter and dance party were giving everyone pedicures  when i barged in on their cabin and even as a relative stranger, insisted I have a foot massage, too! SO awesome :)

Had a funny discussion about how it would be great to make a video about people who are through hiking -- getting to Canada and breaking trekking poles over their knees. Of course through hiking also described what we were all doing (sitting around on leather sofas watching YouTube videos on iPhones, eating fresh baked cookies, avoiding the rain). Is it bad that we're already thinking about being done?
