---
author: ellie
comments: true
date: 2013-02-21 00:39:20+00:00
layout: post
slug: stories-interviewing
title: Stories, Media, & Research Methods
wordpress_id: 1876
tags:
- fieldwork
- interviewing
- media
- sherry turkle
- stories
- technology
---

Last week (in advance of Valentine's Day) NPR asked "There's no doubt that our level of connectedness through smartphones and social media has made it a whole lot easier to reach out and touch those we love - at least virtually. But is there a downside to all this connectedness?" [1]


<!-- more -->


Turning to Sherry Turkle for an answer, we hear first that "it's complicated," but the story quickly shifts to a denouncement of the threats that communications technologies pose to intimate relationships. Staking her authority on "interview[s with] hundreds and hundreds and hundreds of people about their current communications," she reports that, people are [worryingly] "truncating our communications, making them briefer and briefer." She describes our contemporary world as one in which "we're willing to sacrifice conversation for mere connection." She says that "kids today really don't know how to apologize."





As one friend of mine said, such arguments about the inherent problems in "a culture where increasingly I'd rather text than talk" enlist an ablism open to easy critique — who is Turkle (or any of us) to say that intimate relationships require spoken conversation, in particular? And, as another friend pointed out, these arguments also "rely on a rhetoric of sameness and humanism" that we might similarly [easily] critique.





I agree with both of these critiques, but my primary interest at the moment is in the ways that the argument of change is premised on these interviews of the moment, and not in historical research. I have no doubt that, in retrospect, the past is described as 'better' than the present by Turkle's interviewees. But, the interesting question to me, seems to be why this is the case? 





New technologies are often received with a mix of praise and condemnation. Are Turkle's interviewees doing anything more than repeating the stories they might have already heard? What is our responsibility as researchers in presenting such stories as authoritative truth without other historical evidence? When we're dealing with new and emerging technologies, how do we talk about change responsibly? And accurately?





Taking mobile phones as still relatively-new devices, we might learn something from studies about the first users of the Internet. As reported by Shklovski et al in 2002 [2], the first studies of Internet use showed that the "frequency of Internet use was associated with increases in depression and social isolation (Kraut et al., 1998) and declines in spending time with family and friends and in attending social events (Nie et al., 2002)." Yet, later studies, did not always replicate this result, leading to questions about whether the Internet itself leads to depression and social isolation, or whether there was some confounding factor related to its very early adoption. Shklovski et al warn "that cross-sectional methods for evaluating the impact of using the Internet should be treated with caution and augmented or even replaced with longitudinal designs."





So from this methodological perspective, when is it ok to make broad claims about a large-scale temporally-defined "we" [as an entire country/society/class/people/human race — I am not sure what Turkle's field site is, as it seems only ever described as 'we']? And when should we refrain from such moves? What kinds of things can we responsibly state, with authority, based on interview-based studies?





In addition to the question about history, I have some concerns with this kind of interview-based claim-making related to culture, media, and storytelling. In my own ongoing fieldwork with southern California professionals (and their families), I hear a lot about people's concerns with mobile technologies — concerns that seem not terribly different from concerns raised about previous technologies, from books to televisions.





As one participant, Mark, explained to me, he felt good about using his Kindle because, in contrast to his iPad, it "feels virtuous." Later in the interview, his wife, Bobbi, reflected on the virtues of reading a book while 'disconnected' from her technology for an evening, provoking Mark to point out that "if we were we pre electronics, if she went into her study and read I might be resentful and say, ‘You’re taking yourself away from the family. You’re not interacting with the rest of us.’ [… But, now …] our generation feels the sorrow, the loss of the value of reading. So we're mourning that last chapter with this new chapter in front of us, but if we didn't have the new chapter, we'd say, '[Bobbi], why do you read so much?'"





Yet, their reflectiveness about their own historical moment, didn't make them any less concerned about their contemporary technology use. Indeed, they solicited advice and opinion on all manner of concerns. Bobbi worried about her teenage nieces and nephews, so interested in their smartphones at family gatherings, that they seemed to lack the social skills to interact with adults. She worried about the damaging effects of computers and television on her own elementary-school-aged kids' social and cognitive development. She worried about her own 'addiction' to the 'dopamine hits' she thought she was getting every time her phone buzzed to let her know of a new email or text. She and Mark both worried about what it meant to be 'present' for family with the pervasiveness of mobile technologies in their daily life. More often than not, when they brought up these concerns to me, they often included an explicit reference to something they had recently read on the New York Times, or heard on NPR.





Which leads me to a slightly different set of questions with regards to the evidence we get when we conduct interviews. What does it mean to engage in fieldwork with individuals who are always already prepared with well-circulated stories of affective experience? How is the public taken up and articulated in moments of ordinary living? In studying new technologies, how can we carefully examine the mutual shaping of publics, publishers, technological artifacts, and individual experience?





References:  

[1] [http://www.npr.org/templates/transcript/transcript.php?storyId=171490660](http://www.npr.org/templates/transcript/transcript.php?storyId=171490660)  

[2] [http://jcmc.indiana.edu/vol10/issue1/shklovski_kraut.html](http://jcmc.indiana.edu/vol10/issue1/shklovski_kraut.html)
