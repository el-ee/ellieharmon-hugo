---
author: ellie
comments: true
date: 2013-01-29 18:04:40+00:00
layout: post
slug: politics-pleasure-and-desire
title: '"a politics that appeals to pleasure and desire"'
wordpress_id: 1511
tags:
- always on
- criticism
- desire
- ethic
- kathi weeks
- life
- melissa gregg
- pleasure
- technology
- values
- work
---

> ...we should not equate the work ethic with some kind of moral virtue. Instead, I follow Weeks in proposing that we must be open to "a politics that appeals to pleasure and desire, rather than to sacrifice and asceticism," one which "simultaneously takes up the cause of wage laborers while undermining their identity as wage laborers."





- Peter Fraser, opening editorial, "Modify Your Disent," [Winter 2013 Jacobin](http://jacobinmag.com/issue/modify-your-dissent/)


<!-- more -->


In my fieldwork with working professionals in southern California, I find myself frequently both impressed, and somewhat terrified, of the amount of time people are spending doing various kinds of 'work.' When I say terrified, I should be clear that I mean a kind of personal fear and anxiety in trying to imagine myself -- a PhD Candidate for whom graduation and post-school career is looming on an ever closer horizon; a just turned 30 adult, whose friends are seemingly all starting to have babies and move out of rented apartments and into purchased 4 bedroom houses, all the while, both parents in these dual-income families struggling to climb ever higher on the corporate ladder -- How am I going to ever _DO_ all of the many things I see these people doing in each of a single day's time? I don't see myself being one of these people that gets by on 6 hours or less of sleep a night, or gets up at 3am (true story from fieldwork) in order to find time to exercise.





Sometimes ICT use is especially central to all of the working that I see people doing. As is frequently reported in the popular press, people are increasingly 'always on [call]' to work colleagues, clients, and bosses. Sales people report closing big deals, ahead of the competition, only because they were attentive to their work email at 10pm at night. Defense attorneys struggle with how to be `present' for a child's little league baseball game or around the family dinner table, when clients interrupt needing advice in the midst of a "9-11 situation." In her recent book, [Work's Intimacy](http://www.polity.co.uk/book.asp?ref=9780745650272), [Melissa Gregg](http://homecookedtheory.com/) writes about similar situations in terms of "presence bleed" -- a mixing of work with other parts of life that comes along with online (and importantly mobile) technologies.





The increasing pervasiveness of personal mobile communication technologies are certainly part of what seem to be shifts in expectations of others' responsiveness and accessibility that cyclicly fuel this "presence bleed." For many people, clear spatial or temporal boundaries of a "work day" are only a thing of a nostalgic past. However, this is not the only thing that makes me feel both in awe and terrified of the lives of some of my participants. I also see people working 10+ hour days, 5 days a week, and commuting 1-2 hours on either side of that. Even if they seem to really "be at home" when they're at home (e.g. they don't check email at the dinner table, they don't take their laptop to bed), many people are only at home for 10-12 hours a day. After you take out the time needed for basic life necessities -- getting ready for work, cooking dinner, wrangling the kids into doing their homework, dropping the kids off or picking them up from daycare, eating dinner and eating breakfast... precious little time is left for sleep, much less personal pursuits, community activities, and any personal intimate connection with partners, children, extended family, or friends. 





It's both of these situations that I would deem as kinds of 'over-work,' and I find myself struggling with how to talk and write about them in ways that come across as honest but not contemptful or overly judging. I don't think any of the people who fall into these stories are "doing it wrong" -- I feel a bit more like my gut reaction of anxiety to these ways of living is wholly abnormal. It's hardly just my fieldwork participants who are working -- whether physically in the office or on their phones from anywhere but -- what I see as obscene hours. Most of my friends, family, and colleagues seem to be in the same boat. And, in my own struggles to set boundaries and limits for the amount of time I work at my PhD, I'm well aware of how little choice it feels like one has in most decisions about how much or when to work.





So from this context, I really like the way that [Kathi Weeks](http://womenstudies.duke.edu/people?Gurl=&Uil=1183&subpage=profile), in [The Problem With Work](http://www.dukeupress.edu/Catalog/ViewProduct.php?productid=48492), articulates that what is perplexing is not so much that people are "resigned" to a current state of affairs that involves low pay, under/un/precarious employment, and excessive overworking. Rather what's perplexing is that many people are seemingly neither resigned, nor questioning or concerned. Instead, there is a seemingly pervasive "willingness to live for work." And, big picture, this is a question, a perplexing thing, for me, too. What is it about work that makes people feel comfortable with its seeming domination of their day-to-day lives? For whom is work central? And, for whom is it not?





And to get back to the quote that started this post... I really like that Peter Fraser highlights of Weeks' call to a politics of desire and pleasure, and the way that technology is wholly de-centered in this analysis. The point can't be just criticism of any one or any thing, but a more forward-looking imagination that helps seed new stories of happiness and living. What stories can we tell about 'the good life' that aren't grounded in the same accounting-based tropes of 'balance' or that lead towards ever-more-sophisticated self-regulation. What would a politics of work-life-love look like that tried to position itself in opposition to regulatory ideals embedded in productivity tools like Getting Things Done -- one proposed, but, I would say, troubled solution for many who feel overworked? If we don't want to look towards efficiency as a way to supposedly conjure more time, or tired calls for class awakening that seem to figure workers as just cogs/machines, what other metaphors, what forms of pleasure and desire might we draw upon?
