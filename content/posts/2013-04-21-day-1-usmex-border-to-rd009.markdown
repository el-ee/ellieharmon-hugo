---
author: ellie
comments: true
date: 2013-04-21
layout: post
slug: day-1-usmex-border-to-rd009
title: 'Day 1: US/Mex Border to RD009'
wordpress_id: 1997
tags:
- PCT
---

This blogging thing is going to be a challenge. Much prefer writing i my little paper notebook in he evenings. But i have some spare time at Lake Morena and thought I'd catch up for those of you tuning in from home. Its only going to get worse the more I have to type in a batch, eh?

[![image](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-wp-1366675124721.jpg)](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-wp-1366675124721.jpg)

<!-- more -->

So..yesterday.. Lynn dropped us off at the border around 2pm -- a bit late for thru hiker standards and later than I'd hoped but in the end I think it was actually a good thing.

Big thanks to Lynn not only for the ride but also for driving us all over San Diego along its crazy every-road-is-a-limited-access-freeway-streets. Last minute stops at adventure 16 (hat gloves terekking poles) and Walgreen's (sunscreen). Before that a super tasty send off brunch with her and lilly and chris at snooze in SD.

So yes.. Hiking commenced around 2pm. Met someone who's planning to start on Wednesday -- she was there checking it out and took our picture at the monument. We signed into the log as #251 and #252 on the year--its  busy out here already!

[![image](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-IMG_20130421_135622.jpg)](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-IMG_20130421_135622.jpg)

[![image](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-IMG_20130421_140428.jpg)](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-IMG_20130421_140428.jpg)

Thanks to our late start we lucked out on hiking much of the last few hours along the east side of a ridge in the shade!!

A few miles in ran into a guy with 2 donkeys who had an amazing trip planned. Up the PCT to cascade locks and then out the Oregon trail and then down something all the way to Chile! I remarked something along those lines and he said, no not an adventure, a way of life. He also suggested taking it slow which we're already on board with. Only planning to average about 11 miles a day this week and maybe just a hit more than that next week. His reasoning was to prevent getting burned out. I'm more worried about my knees. But no trouble yet!

I was surprised by all the flowers and green grasses etc. This is a different desert than j tree or the places in Arizona with the saguaros. Haven't really hiked areas like this before. Despite there being NO water out there in the creeks, there is clearly water somewhere under the dirt.

lots of city infrastructure. More than I expected. Jeep roads and railways. We waved at some people on a very tourist looking passenger train right near the border.

Lots of birder patrol people. Some asking what we were up to -- as if it wasn't obvious? That was my first offended thought anyway.. We have so much gear. We are on the pct at this time of year. They must see hundreds of thru hikers. Why the interrogation? I dunno. Maybe its egalitarian of them to question the white girl with $500 of specialized gear on her back, too.

More on that later. This area near the border is a weird place to be..

Camped about nine miles in along a seemingly abandoned dirt road around 7pm. Plenty of light to get set up and eat dinner :)

other stuff for day 1:

[![image](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-IMG_20130421_143926.jpg)](http://ellieharmon.com/wp-content/uploads/2013/04/wpid-IMG_20130421_143926.jpg)





  * collected then first dirt samples for my project with Christina.


  * really loving the half mile android app. It's like a data book only interactive. Who would've guessed...


  * surprising amount of cell service. Have been seeing all your likes and hearts on instagram but having a hard time getting strong enough signal to upload more.
