---
author: ellie
comments: true
date: 2013-04-10 19:58:24+00:00
layout: post
slug: we-have-a-lot-of-new-gear
title: We have a lot of new STUFF
wordpress_id: 1938
tags:
- connectivity
- fear
- hiking
- personal locator beacons
- safety
- slowing down
- speed
- technology
- PCT
---

Lilly has been bugging me to write down some things about packing and getting ready for hiking... so, here are some much overdue thoughts on at least one of those things that I jotted down while sitting at a cafe and waiting on an oil change last week. We have bought a LOT of new gear. Maybe an excessive amount.




<!-- more -->



When I did my AT hike a few years ago, the only new thing I bought up front was a sleeping bag. Of course, I replaced my pack and tent along the way (and just a few hundred miles in). Added some stuff sacks. Sent a few things home. But, for the most part, even the new stuff was pretty standard -- an Osprey Pack from REI, a REI brand sleeping bag, the sea to summit silnylon bags. The only fancy thing was a TarpTent Contrail (which I loved).





This time has been different. Luke, especially, has spent some time looking into all kinds of stuff. We bought a Bearikade last summer before our JMT hike. Inspired by someone we met there, Luke (who can never find a pack that fits right), spent several months custom-ordering a McHale backpack. We got some Dirty Girl gaiters after seeing folks with them on the JMT (and SO TOTALLY LOVED them in a recent hike at JTree). We've also added a 2-person quilt from ZPacks (which we mostly like but the footbox is too tiny for both of us to share comfortably. Modifications are in the works!!). We've invested in the trendy Sawyer water filters. We bought ultra-light (ultra-expensive) down jackets from mont-bell. We've even got fleece-lined cuben fiber stuff sacks for pillows!





And we have lots of gadgets -- smartphones, cameras (a Cannon s100 for me and a GoPro for Luke), he will bring a Nexus 7 tablet for reading and writing, and I will bring a little USB recorder in the hopes that at least a few people will let me interview them for my research. With all of that, we also decided to invest in a Solar Panel and battery system (a GoalZero Switch8).





But one thing we decided against was a personal locator beacon (PLB) of some form or another. This issue of safety that these devices aim to address seems to be a frequent topic of people around me -- from hikers, to advisers, to family, to friends. But, I remain unconvinced. A few weeks ago, I read what seemed, at first, to be [a rather compelling argument for bringing one](http://thenewnomads.com/?p=1582). She wrote:





> I was taken off the Pacific Crest Trail this year by having a subarachnoid hemorrhage stroke, a big fat nasty I-have-no-idea-who-I-am brain explosion, on the side of a mountain in the Sierras. There was no cell reception. There were no available carrier pigeons or owls. There were other hikers, but it was at least a day’s walk to the closest civilization. Blood was POURING into my skull at a repulsive rate and because I didn’t have a SPOT, the poor souls in charge of my survival had to get very creative about getting me necessary medical attention. They did, but it took 10 hours. 10 HOURS! According to all current scientific data, there was no way I could’ve survived 10 HOURS of brain hemorrhage without help. I did. It was a miracle. Really, it was. And the gratitude I want to express for the miracle I received is a responsibility to my own well-being as much as possible from this moment in time forward. I will carry a SPOT. I AM badass. But I will carry a SPOT. And I will be ridiculously grateful for the luxury of doing so.





Point being, some random emergency could happen, and you could likely be out of cell phone range. This could save your life.... and then I thought for a few more minutes, and realized, that the thing is, there's nothing terribly different for me about being on the trail and being in my regular life. If I had a freak stroke in my apartment where I live alone much of the time, I would die there, too. If I was having a stroke, it wouldn't matter if I had cell phone reception (which I used to not have in my old apartment), because I might not be able to get to my phone and use it on my own. And there would be no one to save me there either.





And, I can't spend my regular life being worried all the time about what _could_ happen... I certainly don't carry an emergency beacon everywhere I go. Yes, some freak thing could happen. Yes, I could be alone at the time. Yes, I could be hours from help. But, contra the Boy Scouts, I am rarely "prepared" for everything the world throws my way; I can't live my life fully if I'm always trying to anticipate every potentiality.





And, why would I want to set up _extra_ precautions for hiking? It isn't clear to me that hiking is any more dangerous than walking through the streets of SF (threats of a car or bicycle or motorcycle or bus running me over seemingly equally likely as me being the subject of a violent and random crime), than riding in a car. Why does a long-distance hike inspire these questions in the people around me more than 'ordinary' or a more broadly familiar life? Why is a loss of cellular connectivity so scary to us, today? I frequently travel in and out of cell service zones even in the course of my suburban and urban life.





While I was jotting this down at the cafe, I overheard a conversation next to me about the new features in cars. This guy is going on about how this one car has a blind spot detector that he is really sold on for his next car. This feature to make him somehow "more" safe. But, I'm thinking to myself... yeah, blind spots are a thing when you're driving.. but they're a momentary thing. Can't help but feel like this blind-spot-as-problem is a part of an issue of rushing around too much and disregarding moment-to-moment history. Nothing is in a blind spot very long when you're in a car, and if you're paying attention to the history of your observations as they happen, then it's not really an issue. Slow down. Look. Pause. Wait a second while you _keep looking._ Can we not pay attention to anything for more than two seconds? Can we not keep in mind some idea of the cars around us on the freeway so that we are expecting one to travel in or out of a blind spot? It's like we're all paralyzed by some kind of crazy ADD plague/epidemic... that companies might latch onto in generating fears about the world that can be ameliorated with new gadgets.





I'm hesitant to adapt some of these gadgets into my life, then... but my boundaries about technology are fuzzy at best. I might reason about any one particular item -- like the personal locator beacon or a car blind spot detector or a cuben fiber stuff sack --  but it's hard to come up with a way of describing where that boundary lies in a way that is more general than any specific encounter.
