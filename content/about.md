---
title: "About Me"
date: 2019-02-09T15:49:00-08:00

menu:
    main:
        title: "About Me"
---

![photographing infrastructure on the PCT](/images/PCT-infrastructure.jpg)

I grew up in a small town in northeast Georgia. I was educated in the local public school system, where I played clarinet (in the symphony), bass drum (in the marching band), left midfield in soccer, was a proud member of the national art honor society, and was, much to my chagrin, voted 'Most Academic' by my senior class.

I first learned to program in Logo when I was in kindergarten, definitely had a very cool Geocities homepage as a teen, and eventually grew up to be a computer science major at [Georgia Tech][].

Since 2017, I've been teaching [human-computer interaction, computer ethics, and introductory computer science](https://web.cecs.pdx.edu/~harmon8) at Portland State University.

In between my CS degree, and my current job, I've done a bunch of things:

- **2005:** Epic 5-month road trip, through most of the lower 48 US states, camping in over 20 different national and state parks.

- **2005-2007:** Back to Georgia Tech where I worked with philosopher [Nancy Nersessian][] and cultural linguist [Wendy Newstetter][] to examine the role of computing in scientific creativity. It was here that I first learned to practice ethnography.

- **2007-2008:** I designed custom furniture, textiles, and architectural elements at [Seret & Sons][]. I also designed and built the store website, and worked on its inventory system -- in FileMaker Pro!

- **2008:** I [thru-hiked][thru-hike] the 2176-mile [Appalachian Trail][].

- **2008-2009:** I lived in San Francisco, and worked first for the [US Census Bureau][census] -- where I was thoroughly intrigued by the deployment of new "hand held computing (HHC)" devices within the strictures of an impressive government bureaucracy -- and later for [Microsoft's then recent acquisition, Tellme][tellme] -- the Siri for BlackBerries that existed long before Apple popularized their voice-based assistant.

- **2009-2015:** Completed my PhD at UC Irvine, in the department of [Informatics](http://www.informatics.uci.edu/). My [dissertation][diss] examined the affects and experiences of ordinary life in time period marked by the arrival of ubiquitous mobile computing and the emergence of a popular conversation about disconnection and unplugging.

- **2013:** Five years after my AT hike, I returned to the mountains to walk the 2650-mile [Pacific Crest Trail][pct], this time with my partner [Luke][luke].

- **2014:** Created lots of visualizations about the [bacteria that live in the dirt](http://dirtmap.org/) I collected on my PCT hike, in a collaborative science-as-process-art project with [Christina Agapakis](http://agapakis.com).

- **2014:** Built the website for [Method Quarterly](http://www.methodquarterly.com/).

- **2015-2016:** Research associate in [Amy Voida's][amy] research lab working
at [the forefront of philanthropic informatics][pirl].

- **2016-2017:** Consulted for the International Labour Office and IG Metall on
projects
[studying](https://drive.google.com/file/d/1G8s9F3YQFPJslgVQYLcvxNsQZxNKXjQ1/view?usp=sharing)
[working conditions](http://faircrowd.work) on
[online platforms](https://www.ilo.org/global/publications/books/WCMS_645337/lang--en/index.htm).

  [informatics]: https://www.informatics.uci.edu/
  [UCI]: https://uci.edu/

  [Laboratory for Ubiquitous Computing and Interaction (LUCI)]: http://luci.ics.uci.edu/

  [encounteringtech]: http://encountering.tech
  [psu-cs]: http://cs.pdx.edu/~harmon8

  [Georgia Tech]: http://gatech.edu
  [computer science]: http://www.cc.gatech.edu/
  [human-computer interaction (HCI)]: http://mshci.gatech.edu/
  [Nancy Nersessian]: https://www.cc.gatech.edu/aimosaic/faculty/nersessian/
  [Wendy Newstetter]: https://bme.gatech.edu/bme/faculty/Wendy-Newstetter
  [Seret & Sons]: http://seretandsons.com/
  [thru-hike]: http://www.whiteblaze.net/forum/ap.php?tabid=37
  [Appalachian Trail]: http://www.appalachiantrail.org/
  [pictures]: http://www.flickr.com/photos/_ellie_/collections/72157607038055438/
  [tellme]: https://en.wikipedia.org/wiki/Tellme_Networks
  [census]: https://www.census.gov/
  [luke]: https://lukeolbrish.com/
  [amy]: http://amy.voida.com/
  [pirl]: http://amy.voida.com/philanthropic-informatics/

  [infosci]: https://www.colorado.edu/cmci/infoscience
  [boulder]: https://www.colorado.edu/
  [pct]: https://www.pcta.org/
  [diss]: https://escholarship.org/uc/item/1dx9060p
